//
//  SubCategoryListViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 24/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SubCategoryListViewController: UIViewController {
    var subCategoryTitle: String?
    var groupViewModel: GroupViewModel?
    var subcategoryIndex: Int?
    var navigationBarTitle: String?
    private var items: [CategoryItems]?
    private let scrollView = UIScrollView()
    
    //MARK: - Subviews
    private let homeStackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 16)
    private var shopByCategory: CategoryView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shopByCategory = CategoryView(delegate: self, productCategory: CategoryType.category)
        self.navigationItem.title = subCategoryTitle
        view.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        if let vm = self.groupViewModel, let categoryIndex = subcategoryIndex {
            self.items = vm.subGroupForGroupAt(categoryIndex)?.map({
                CategoryItems(name: $0.name, details: nil, image: nil, urlImage: $0.image)
            })
        }
        
        addSubViews()
        applyModel()
        addAutoLayoutToSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = self.navigationBarTitle
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    private func addSubViews() {
        view.addAutolayoutSubview(scrollView)
        
        scrollView.addAutolayoutSubview(homeStackView)
        [shopByCategory].compactMap({ $0 }).forEach {
            homeStackView.addArrangedAutolayoutSubview($0)
        }
    }

    private func applyModel() {
        apply(shopByCategory) {
            $0.numberOfColumns = 2
            $0.cellHeight = 173
            $0.isOfferViewHidden = true
            if let items = self.items {
                $0.items = items
            }
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        }
    }

    private func addAutoLayoutToSubviews(){
        NSLayoutConstraint.activate([
            homeStackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor, constant: 20),
            homeStackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor, constant: 16),
            homeStackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor, constant: -16),
            homeStackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor, constant: -20),
            homeStackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -32)
        ])
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
        ])
    }

}

extension SubCategoryListViewController: CategoryCollectionViewSelection {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType) {
        if let vm = self.groupViewModel, let categoryIndex = subcategoryIndex {
            let subGroups = vm.subGroupForGroupAt(categoryIndex)
            let subGroup = subGroups?[index]
            let productListVC = self.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
            productListVC.items = subGroup
            productListVC.navigationBarTitle = self.navigationBarTitle
            self.navigationController?.pushViewController(productListVC, animated: true)
        }
    }
}

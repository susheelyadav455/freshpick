//
//  SearchConversionHelper.swift
//  Freshpick
//
//  Created by Ajeet N on 06/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

enum SearchCriterias: String {
    case category = "Category"
    case subCategory = "Sub Category"
    case items = "Items"
    case notFound = "Search Item"
}

struct SearchObject{
    let key:String
    let value:[AnyObject]
}

struct SearchConversionHelper {
    func convertModelToKeyValue(model: SearchModel)->[SearchObject]{
        let categories = model.response?.inCategory
        let subCategories = model.response?.inSubCategory
        let items = model.response?.inItem
        
        var searchArray = [SearchObject]()
        if let category = categories, category.count > 0 {
            searchArray.append(SearchObject(key: SearchCriterias.category.rawValue, value: category as [AnyObject]))
        }
        if let subCategories = subCategories, subCategories.count > 0 {
            searchArray.append(SearchObject(key: SearchCriterias.subCategory.rawValue, value: subCategories as [AnyObject]))
        }
        if let items = items, items.count > 0 {
            searchArray.append(SearchObject(key: SearchCriterias.items.rawValue, value: items as [AnyObject]))
        }
        if searchArray.count == 0 {
            searchArray.append(SearchObject(key: SearchCriterias.notFound.rawValue, value: ["No Data Found"] as [AnyObject]))
        }
        return searchArray
    }
}

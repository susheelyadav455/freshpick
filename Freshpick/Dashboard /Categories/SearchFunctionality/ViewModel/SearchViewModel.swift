//
//  SearchViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 06/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SearchViewModel {
    private var handler: ((_ error: String?)->Void)?
    private var searchObjects = [SearchObject]()
    
    func getSearchResult(_ searchString: String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        self.handler = completion
        let url = API.BASEURL+API.SEARCH+"?v=\(searchString)"
        ServiceManager.shared.request(type: SearchModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result {
                print("SearchViewModel result :\(result)")
                self.searchObjects = SearchConversionHelper().convertModelToKeyValue(model: result)
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func numberOfSection()->Int{
        return searchObjects.count
    }
    
    func numberOfRowsForSection(_ at:Int)->Int{
        return searchObjects[at].value.count
    }
    
    func modelForSection(_ at:Int)->SearchObject{
        return self.searchObjects[at]
    }
}

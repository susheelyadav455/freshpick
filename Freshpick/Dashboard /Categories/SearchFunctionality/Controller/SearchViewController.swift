//
//  SearchViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    let items:[CategoryItems] = [
                            CategoryItems(name: "Fruits and Vegetables",details: "Upto 20% Off", image: "Fruits_Veges"),
                               CategoryItems(name: "Personal Care",details: "Upto 20% Off", image: "Personal_Care"),
                               CategoryItems(name: "Food Grains and oils",details: "Upto 20% Off",image: "Food Grains_Oils"),
                               CategoryItems(name: "Bakery Bread & Dairy",details: "Upto 20% Off", image: "Bakery_Bread_Dairy"),
                               CategoryItems(name: "Eggs and Meat",details: "Upto 20% Off", image: "Eggs_Meat"),
                               CategoryItems(name: "Cleaning Household",details: "Upto 20% Off", image: "Cleaning_Housholds"),
                               CategoryItems(name: "Snacks",details: "Upto 20% Off", image: "Snacks"),
                               CategoryItems(name: "Ready to Cook",details: "Upto 20% Off", image: "ready_to_cook"),
                               CategoryItems(name: "Drinks & Juices",details: "Upto 20% Off", image: "Coffee_Tea"),
    ]
    
    @IBOutlet var scrollContainerViewObj: UIView!
//    @IBOutlet var searchBAr: UISearchBar!{
//        didSet{
//            searchBAr.delegate = self
//        }
//    }
    private let searchBar:FPSearchBar = .fromNib()
    @IBOutlet var scrollView: UIScrollView!
    private var isCallSearchApi = false
    private let tableView = UITableView()
    var catV: CategoryView!//(delegate: self)
    @IBOutlet var scrollViewTopConstrain: NSLayoutConstraint!
    
    let searchViewModel = SearchViewModel()
    var tableViewHeight = NSLayoutConstraint()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.catV = CategoryView(delegate: self, productCategory: CategoryType.search)
        navigationItem.title = "Search"
        view.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        scrollContainerViewObj.backgroundColor = UIColor(red: 248, green: 248, blue: 248)

//        self.searchBAr.becomeFirstResponder()
        apply(searchBar) {
            $0.fpSearchBar = self
            $0.setShadow(shadowRadius: 5)
            $0.becomeFirstResponder()
        }
        view.addAutolayoutSubview(searchBar)
//        scrollViewTopConstrain.
        view.removeConstraint(scrollViewTopConstrain)
        catV.items = items
        catV.numberOfColumns = 3
        catV.cellHeight = 173
        catV.isHeaderVisible = true
        catV.isOfferViewHidden = true
        catV.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.layer.cornerRadius = 10
        tableView.register(UINib.init(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: "ExpandableCell")
        tableView.register(UINib.init(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.isScrollEnabled = false
        
        let stackView = UIStackView(alignment: .fill, distribution: .fillProportionally, axis: .vertical, spacing: 0)
        scrollContainerViewObj.addAutolayoutSubview(stackView)
        
        [catV,tableView].forEach {
            stackView.addArrangedAutolayoutSubview($0)
        }
        tableViewHeight = tableView.heightAnchor.constraint(equalToConstant: tableView.contentSize.height)
        NSLayoutConstraint.activate([
            tableView.heightAnchor.constraint(equalToConstant: 2000),
            tableViewHeight,
            stackView.topAnchor.constraint(equalTo: scrollContainerViewObj.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollContainerViewObj.leadingAnchor,constant: 16),
            stackView.trailingAnchor.constraint(equalTo: scrollContainerViewObj.trailingAnchor,constant: -16),
            stackView.bottomAnchor.constraint(equalTo: scrollContainerViewObj.bottomAnchor),
        ])
        tableView.isHidden = true
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            searchBar.bottomAnchor.constraint(equalTo: scrollView.topAnchor, constant: -16),
            searchBar.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//    }
    
    private func changeViewInContainer(searchText: String) {
        if searchText.count > 2 {
            isCallSearchApi = true
            self.toggleSearchView(searchText: searchText)
        } else if searchText.count == 0 {
            tableView.isHidden = true
            catV.isHidden = false
        } else {
            if isCallSearchApi {
                self.toggleSearchView(searchText: searchText)
            }
        }
    }
    
    func toggleSearchView(searchText: String){
        tableView.isHidden = false
        catV.isHidden = true
        callSearchAPI(text: searchText)
    }
    
    func callSearchAPI(text:String){
        self.searchViewModel.getSearchResult(text, controller: self) {[weak self] (result) in
            guard let self = self else {
                return
            }
            if result == nil {
                self.tableView.reloadData()
                self.view.layoutIfNeeded()
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }
        }
    }
}

//extension SearchViewController: UISearchBarDelegate {
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        if let text = searchBar.text {
//            self.toggleSearchView(searchText: text)
//        }
//    }
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if let text = searchBar.text {
//            changeViewInContainer(searchText: text)
//        }
//    }
//}

extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.searchViewModel.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchViewModel.numberOfRowsForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpandableCell", for: indexPath) as! ExpandableCell
        cell.backgroundColor = UIColor(red: 252, green: 252, blue: 252)
        
        let model = self.searchViewModel.modelForSection(indexPath.section)
        switch model.key {
        case SearchCriterias.category.rawValue:
            let cellModel = model.value as! [InCategory]
            cell.cellTitleObj.text = cellModel[indexPath.row].name
            cell.disclosureObj.isHidden = false
            cell.selectionStyle = .default
            break
        case SearchCriterias.subCategory.rawValue:
            let cellModel = model.value as! [InSubCategory]
            cell.cellTitleObj.text = cellModel[indexPath.row].name
            cell.disclosureObj.isHidden = false
            cell.selectionStyle = .default
            break
        case SearchCriterias.items.rawValue:
            let cellModel = model.value as! [InItem]
            cell.cellTitleObj.text = cellModel[indexPath.row].name
            cell.disclosureObj.isHidden = false
            cell.selectionStyle = .default
            break
        case SearchCriterias.notFound.rawValue:
            let cellModel = model.value as! [String]
            cell.cellTitleObj.text = cellModel[indexPath.row]
            cell.disclosureObj.isHidden = true
            cell.selectionStyle = .none
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        cell.backgroundColor = .white
        
        let model = self.searchViewModel.modelForSection(section)
        cell.headerTitleObj.text = model.key

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.searchViewModel.modelForSection(indexPath.section)
        switch model.key {
        case SearchCriterias.category.rawValue:
            let cellModel = model.value as! [InCategory]
            break
        case SearchCriterias.subCategory.rawValue:
            let cellModel = model.value as! [InSubCategory]
            let productListVC = self.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
            productListVC.items = cellModel[indexPath.row]
            productListVC.navigationBarTitle = cellModel[indexPath.row].name
            self.navigationController?.pushViewController(productListVC, animated: true)
            
            break
        case SearchCriterias.items.rawValue:
            let cellModel = model.value as! [InItem]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
            vc.item = cellModel[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
            
            break
        default:
            break
        }
    }
}

extension SearchViewController: CategoryCollectionViewSelection {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType) {
        print("SearchViewController selected category :\(index) items :\(item) productCategory :\(productCategory)")
    }
}

extension SearchViewController: FPSearchBarDelegate{
    func searchBarShouldBeginEditing(_ textField: UITextField, _ shouldEdit: Bool) {
        
    }
    
    func searchButtonClicked(_ text: String) {
        print("HomeVC Search Button clicked :\(text)")
        if !text.isEmpty {
            self.toggleSearchView(searchText: text)
        }
    }
    
    func searchBarText(_ text: String) {
        print("HomeVC Search text :\(text)")
        if !text.isEmpty {
            changeViewInContainer(searchText: text)
        }
    }
}

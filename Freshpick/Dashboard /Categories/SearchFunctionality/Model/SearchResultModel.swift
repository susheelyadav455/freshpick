//
//  SearchResultModel.swift
//  Freshpick
//
//  Created by Ajeet N on 06/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

// MARK: - SearchModel
struct SearchModel: Codable {
    let code: Int?
    let response: Response?
}

// MARK: - Response
struct Response: Codable {
    let inCategory: [InCategory]?
    let inSubCategory: [InSubCategory]?
    let inItem: [InItem]?
}

// MARK: - InCategory
struct InCategory: Codable {
    let id: Int?
    let name: String?
    let deleted: Bool?
    let createdAt, updatedAt: String?
}

// MARK: - InItem
struct InItem: Codable {
    let id: Int?
    let mrp: Int?
    let sellingPrice: DoublePrice?
    let manufacturer, createdAt, updatedAt: String?
    let productQuantityID: Int?

//    let name, brandCode, subGroupCode, groupCode: String?
//    let quantity: String?
//    let image: String?
//    let details: String?
//    let deleted: Bool?
    
    let productCode: Int?
    let name: String?
    let image: String?
    let brandCode: String?
    let quantity: StringQuantity?
    let isAssured: Bool?
    let productQuantityCode: Int?
    let weightOrQuantity: String?
    let amount, actualAmount: Int?
    let brand, subGroup, subGroupCode, groupCode: String?
    let details, eanCode: String?
    let disableReview, outOfStock, isApproved: Bool?
    let donationType, donation: String?
    let deleted: Bool?
    let extraDiscount: Int?
    let finalAmount: Double?

    enum CodingKeys: String, CodingKey {
        case id
        case mrp, sellingPrice, createdAt, updatedAt
        case productQuantityID = "productQuantityId"
        
        case productCode, name, image, brandCode, quantity, isAssured, productQuantityCode, weightOrQuantity, amount, actualAmount, brand, subGroup
        case subGroupCode = "sub_groupCode"
        case groupCode, details
        case eanCode = "EANCode"
        case disableReview, outOfStock, isApproved, donationType, donation, deleted, manufacturer, extraDiscount, finalAmount
    }
}

// MARK: - InSubCategory
struct InSubCategory: Codable {
    let id: Int?
    let name: String?
    let image: String?
    let groupCode: String?
    let deleted: Bool?
    let createdAt, updatedAt: String?
}

enum StringQuantity: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Quantity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

struct DoublePrice: Codable {
    let price: Double?

    // Where we determine what type the value is
    init(from decoder: Decoder) throws {
        let container =  try decoder.singleValueContainer()

        // Check for a Double
        if let x = try? container.decode(Double.self) {
            price = x
            return
        }
        
        // Check for a Int
        if let x = try? container.decode(Int.self) {
            price = Double(x)
            return
        }
        throw DecodingError.typeMismatch(Response.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    // We need to go back to a dynamic type, so based on the data we have stored, encode to the proper type
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(price)
    }
}

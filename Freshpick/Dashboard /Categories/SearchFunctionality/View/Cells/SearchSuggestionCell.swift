//
//  SearchSuggestionCell.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SearchSuggestionCell: UICollectionViewCell {

    @IBOutlet var contentViewObj: CustomView!
    @IBOutlet var imageObj: UIImageView!
    @IBOutlet var titleObj: UILabel!
    @IBOutlet var offerObj: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        contentViewObj.layer.masksToBounds = true
        contentViewObj.layer.cornerRadius = 10
//        contentViewObj.setDefaultShadow()
        contentView.backgroundColor = .clear
        self.backgroundColor = .clear
    }

}

//
//  ExpandableCell.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ExpandableCell: UITableViewCell {
    @IBOutlet var disclosureObj: UIImageView!
    
    @IBOutlet var cellTitleObj: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

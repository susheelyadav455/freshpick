//
//  AvailableSlotView.swift
//  Freshpick
//
//  Created by Ajeet N on 12/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class AvailableSlotView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private let firstLineView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let stackView: UIStackView = {
        let view = UIStackView()
        return view
    }()
    
    private let slotLabel: UILabel = {
        let view = UILabel()
        return view
    }()
    
    private let slotTextLabel: UILabel = {
        let view = UILabel()
        return view
    }()
    
    private let lastLineView: UIView = {
        let view = UIView()
        return view
    }()
    
    private func setupView() {
//        backgroundColor = .blue
        addAutolayoutSubview(stackView)
        stackView.addArrangedAutolayoutSubviews([firstLineView, slotLabel, slotTextLabel, lastLineView])
        
        apply(stackView) {
            $0.axis = .vertical
            $0.distribution = .fillProportionally
            $0.alignment = .fill
            $0.spacing = 10
        }
        
        apply(firstLineView) {
            $0.backgroundColor = UIColor(red: 0, green: 0, blue: 0, a: 0.1)
        }
        
        apply(lastLineView) {
            $0.backgroundColor = UIColor(red: 0, green: 0, blue: 0, a: 0.1)
        }
        
        apply(slotLabel) {
            $0.text = "Your next available slot"
            $0.textColor = kDarkTextColor
            $0.font = UIFont(name: kMuliBold, size: 15)
            $0.numberOfLines = 0
        }
        
        apply(slotTextLabel) {
            $0.text = "Tomorrow between 11:00 to 13:00"
            $0.textColor = kDescriptionTextColor
            $0.font = UIFont(name: kMuliBold, size: 14)
            $0.numberOfLines = 0
        }
        
        NSLayoutConstraint.activate([
            firstLineView.heightAnchor.constraint(equalToConstant: 1),
            lastLineView.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
}

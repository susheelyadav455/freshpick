//
//  ItemBrandAndPriceDetailsView.swift
//  Freshpick
//
//  Created by Ajeet N on 11/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ItemBrandAndPriceDetailsView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    let brandName: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let price: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private func setupView() {
        addAutolayoutSubview(brandName)
        addAutolayoutSubview(price)

        apply(brandName) {
            $0.numberOfLines = 0
            $0.text = """
            
            Apple is an equal opportunity employer that is committed to inclusion and diversity. We also take affirmative action to offer employment and advancement opportunities to all applicants. Apple is committed to working with and providing reasonable accommodation to applicants with physical and mental disabilities. Apple is a drug-free workplace.
"""
            $0.setContentHuggingPriority(.init(rawValue: 245), for: .horizontal)
        }
        
        apply(price) {
            $0.text = "3456 3456 3456"
            $0.setContentCompressionResistancePriority(.init(rawValue: 752), for: .horizontal)
        }
        
        NSLayoutConstraint.activate([
            brandName.topAnchor.constraint(equalTo: self.topAnchor),
            brandName.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            brandName.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            price.topAnchor.constraint(equalTo: self.topAnchor),
            price.leadingAnchor.constraint(equalTo: brandName.trailingAnchor, constant: 20),
            price.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            price.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}

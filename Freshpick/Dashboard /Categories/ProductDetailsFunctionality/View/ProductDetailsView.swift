//
//  ProductDetailsView.swift
//  Freshpick
//
//  Created by Ajeet N on 09/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
struct ProductDetailsModel {
    var images:[String?]
    var manufacturer: String?
    var mrp: String?
    var sellingPrice: String?
    var quantity: String?
    var details: String?
    var name: String?

//    "manufacturer": "Dole Food Company, Inc.",
//    "mrp": 30,
//     "sellingPrice": 25,
//     "quantity": "500g",
//    "details": "fekogruc",
}

class ProductDetailsView: UIView {
    var model: ProductDetailsModel?{
        didSet{
            updateModel()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    weak var quantityDelegate: QuantityDelegate?
    weak var stepperDelegate: StepperDelegate?

    private let slotView = AvailableSlotView()
    private let itemDetails = ItemDetailsView()
    private let groceryQuantitySelectionView = QuantitySelectionView()
    private let itemBrandAndprice = ItemBrandAndPrice()
    private let imageCrausalView = ImageCrausalView()

    
    private func viewSetup(){
        self.setShadow()
        self.backgroundColor = .white
        
        addAutolayoutSubview(imageCrausalView)
        addAutolayoutSubview(itemBrandAndprice)
        addAutolayoutSubview(groceryQuantitySelectionView)
        addAutolayoutSubview(slotView)
        addAutolayoutSubview(itemDetails)
        
        groceryQuantitySelectionView.quantityDelegate = self
        groceryQuantitySelectionView.stepperDelegate = self

        
        NSLayoutConstraint.activate([
            imageCrausalView.topAnchor.constraint(equalTo: self.topAnchor,constant: 20),
            imageCrausalView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageCrausalView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            itemBrandAndprice.topAnchor.constraint(equalTo: imageCrausalView.bottomAnchor),
            itemBrandAndprice.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            itemBrandAndprice.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            itemBrandAndprice.bottomAnchor.constraint(equalTo: groceryQuantitySelectionView.topAnchor, constant: -20),
        ])
        
        NSLayoutConstraint.activate([
            groceryQuantitySelectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            groceryQuantitySelectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            groceryQuantitySelectionView.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            slotView.topAnchor.constraint(equalTo: groceryQuantitySelectionView.bottomAnchor, constant:  10),
            slotView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            slotView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
        ])
        
        NSLayoutConstraint.activate([
            itemDetails.topAnchor.constraint(equalTo: slotView.bottomAnchor, constant: 5),
            itemDetails.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            itemDetails.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            itemDetails.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
        ])
    }
}

extension ProductDetailsView: QuantityDelegate{
    func quantitySelected(sender: UIButton) {
        quantityDelegate?.quantitySelected(sender: sender)
    }
}

extension ProductDetailsView {
    func updateModel(){
        imageCrausalView.images = model?.images
        itemBrandAndprice.model = ItemBrandAndPriceModel(brandName: model?.name, mrp: model?.mrp, sellingPrice: model?.sellingPrice, manufacturer: model?.manufacturer)
        groceryQuantitySelectionView.model = QuantitySelectionModel(quantity: model?.quantity)
        itemDetails.model = ItemDetailsModel(details: [model?.details,model?.manufacturer])
    }
}

extension ProductDetailsView: StepperDelegate{
    func numberOfItems(count: Int) {
        stepperDelegate?.numberOfItems(count: count)
    }
}

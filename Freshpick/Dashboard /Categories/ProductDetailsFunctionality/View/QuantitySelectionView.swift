//
//  QuantitySelectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 11/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol QuantityDelegate:class {
    func quantitySelected(sender: UIButton)
}

struct QuantitySelectionModel {
    var quantity: String?
}

class QuantitySelectionView: UIView {
    weak var stepperDelegate: StepperDelegate?
    var model: QuantitySelectionModel?{
        didSet{
            applyModel()
        }
    }
    weak var quantityDelegate: QuantityDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private let quantityButton: UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    private let stepper: GroceryStepper = {
        let vw = GroceryStepper()
        return vw
    }()
    
    private func viewSetup(){
        addAutolayoutSubview(quantityButton)
        addAutolayoutSubview(stepper)
        
        stepper.stepperDelegate = self
        
        apply(quantityButton) {
            $0.setTitle("500g", for: .normal)
            $0.setTitleColor(kRedColor, for: .normal)
            $0.titleLabel?.font = UIFont(name: kMuliBold, size: 15)
            $0.addTarget(self, action: #selector(self.qunatityButtonAction(sender:)), for: .touchUpInside)
        }
        
        NSLayoutConstraint.activate([
            quantityButton.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            quantityButton.heightAnchor.constraint(equalToConstant: 50),
            quantityButton.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stepper.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stepper.heightAnchor.constraint(equalToConstant: 40),
            stepper.widthAnchor.constraint(equalToConstant: 250),
            stepper.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
    
    @objc func qunatityButtonAction(sender: UIButton){
        quantityDelegate?.quantitySelected(sender: sender)
    }

}

extension QuantitySelectionView{
    func applyModel(){
        apply(quantityButton) {
            $0.setTitle(model?.quantity, for: .normal)
        }
    }
}

extension QuantitySelectionView: StepperDelegate{
    func numberOfItems(count: Int) {
        self.stepperDelegate?.numberOfItems(count: count)
    }
}

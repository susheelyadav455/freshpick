//
//  ItemDetailsView.swift
//  Freshpick
//
//  Created by Ajeet N on 14/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

struct ItemDetailsModel {
    var details: [String?]
}

class ItemDetailsView: UIView {
    var model:ItemDetailsModel?{
        didSet{
            updateModel()
        }
    }
    let textView = UITextView()
    private var selectedIndex = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func viewSetup() {
        let collectionView = FPThemeCollectionView(collectionViewLayout: UICollectionViewFlowLayout())
        addAutolayoutSubview(collectionView)
        collectionView.fpThemeDelegate = self
        collectionView.items = ["Product Details","Manufacture"]
        collectionView.selectItem(at: IndexPath.init(row: selectedIndex, section: 0), animated: true, scrollPosition: .left)

        addAutolayoutSubview(textView)

        textView.setTextViewTextWithAttributed(string: prepareLink(with: (model?.details.first ?? "") ?? "", linkButton: " Read more..", takeNumberOfWords: 20), tintColor: kRedColor)
        textView.sizeToFit()
        textView.isSelectable = true
        textView.isEditable = false
        textView.delegate = self
        textView.isScrollEnabled = false
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: collectionView.bottomAnchor),
            textView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            textView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
    
    func prepareLink(with str1:String,linkButton str2: String, takeNumberOfWords:Int) -> NSMutableAttributedString {
        let mainString = get(first: 10, words: str1)
        let firstStringColor = kDescriptionTextColor
        let attributedStringColor = [NSAttributedString.Key.foregroundColor : firstStringColor,
                                     NSAttributedString.Key.font: UIFont(name: kMuliMedium, size: 14)
        ];
        let attributedString = NSAttributedString(string: mainString.0, attributes: attributedStringColor as [NSAttributedString.Key : Any])
        
        let attrString = NSMutableAttributedString.init(attributedString: attributedString)
        
        if mainString.1 > 10 {
            let formattedString = NSMutableAttributedString(string: str2)
            let linkAttribute = [
                NSAttributedString.Key.foregroundColor: kRedColor,
                NSAttributedString.Key.font: UIFont(name: kMuliMedium, size: 14) as Any,
                NSAttributedString.Key.link: URL(string: "http://www.google.com")!
                ] as [NSAttributedString.Key : Any]
            formattedString.addAttributes(linkAttribute, range: NSMakeRange(0, formattedString.length))
            
            attrString.append(formattedString)
        }
        
        return attrString
    }
    
    @discardableResult func get(first words: Int, words From: String)->(String,Int){
        var mutStr = ""
        let wd = From.components(separatedBy: " ")
        var strArr = [String]()
        if wd.count > words {
            for index in 0...words {
                strArr.append("\(wd[index])")
            }
            mutStr = strArr.joined(separator: " ")
            return (mutStr,words)
        } else {
            return (From,wd.count)
        }
    }
}

extension ItemDetailsView{
    func updateModel(){
        self.textView.setTextViewTextWithAttributed(string: prepareLink(with: (model?.details.first ?? "") ?? "", linkButton: " Read more..", takeNumberOfWords: 20), tintColor: kRedColor)
    }
}

extension ItemDetailsView: CellSelectionDelegate {
    func cellSelected(indexPath: IndexPath, item: AnyObject) {
        selectedIndex = indexPath.row
        self.textView.setTextViewTextWithAttributed(string: prepareLink(with: model?.details[indexPath.row] ?? "", linkButton: " Read more..", takeNumberOfWords: 20), tintColor: kRedColor)
    }
}

extension ItemDetailsView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        self.textView.setTextViewTextWith(string: model?.details[selectedIndex] ?? "", tintColor: kDescriptionTextColor)
        return false
    }
}

extension UITextView {
    func setTextViewTextWithAttributed(string: NSAttributedString, tintColor: UIColor){
        apply(self) {
            $0.attributedText = string
            $0.tintColor = tintColor
        }
    }
    
    func setTextViewTextWith(string: String, tintColor: UIColor){
        apply(self) {
            $0.text = string
            $0.tintColor = tintColor
        }
    }
}

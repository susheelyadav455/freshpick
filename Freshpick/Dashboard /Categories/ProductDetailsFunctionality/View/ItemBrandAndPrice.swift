//
//  ItemBrandAndPrice.swift
//  Freshpick
//
//  Created by Ajeet N on 11/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
struct ItemBrandAndPriceModel {
    var brandName: String?
    var mrp: String?
    var sellingPrice: String?
    var manufacturer: String?
    
}

class ItemBrandAndPrice: UIView {
    var model: ItemBrandAndPriceModel?{
        didSet{
            updateModel()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private let itemTitleView = ItemBrandAndPriceDetailsView()
    private let itemView = ItemBrandAndPriceDetailsView()
    
    private func setupView() {
        apply(itemTitleView) {
            $0.brandName.text = "Brand name"
            $0.brandName.textColor = kDarkTextColor
            $0.brandName.font = UIFont(name: kMuliBold, size: 11)
            $0.brandName.numberOfLines = 0
            
            $0.price.attributedText = "Rs.180".strikeThrough()
            $0.price.textColor = kDescriptionTextColor
            $0.price.font = UIFont(name: kMuliBold, size: 11)
            $0.brandName.numberOfLines = 0
        }
        
        self.addAutolayoutSubview(itemTitleView)
        
        apply(itemView) {
            $0.brandName.text = "Fresh Imported Mangoes"
            $0.brandName.font = UIFont(name: kMuliBold, size: 14)
            $0.brandName.textColor = kDarkTextColor
            $0.brandName.numberOfLines = 0
            
            $0.price.textColor = kDarkTextColor
            $0.price.font = UIFont(name: kMuliBold, size: 15)
            $0.price.text = "Rs.149"
            $0.brandName.numberOfLines = 0
        }
        
        self.addAutolayoutSubview(itemView)
        
        NSLayoutConstraint.activate([
            itemTitleView.topAnchor.constraint(equalTo: self.topAnchor),
            itemTitleView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            itemTitleView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
            itemTitleView.bottomAnchor.constraint(equalTo: itemView.topAnchor),

        ])
        
        NSLayoutConstraint.activate([
            itemView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            itemView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
            itemView.bottomAnchor.constraint(equalTo: self.bottomAnchor),

        ])
    }
}

extension ItemBrandAndPrice{
    func updateModel(){
        apply(itemTitleView) {
            $0.brandName.text = model?.manufacturer
            $0.price.attributedText = "Rs.\(model?.mrp ?? "")".strikeThrough()
        }
        apply(itemView) {
            $0.brandName.text = model?.brandName
            $0.price.text = "Rs.\(model?.sellingPrice ?? "")"
        }
    }
}

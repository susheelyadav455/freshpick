//
//  RelatedItemsTableView.swift
//  Freshpick
//
//  Created by Ajeet N on 12/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
struct Items {
    let imageName: String
    let name: String
    let quantity: String
    let originalPrice: String
    let discountedPrice: String
}

class RelatedItemsTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    var headerButtonTitle: String?
    var items = [Items]() {
        didSet{
            self.reloadData()
            print("===== Size of content :\(self.contentSize.height)")
        }
    }
    
//    var item = [InItem]() {
//        didSet{
//            self.reloadData()
//            print("===== Size of content item :\(self.contentSize.height)")
//
//        }
//    }
    
    var headerTitles = [String]() {
        didSet {
            self.reloadSections(IndexSet.init(arrayLiteral: 0), with: .automatic)
        }
    }
    
//    override var intrinsicContentSize: CGSize {
//        self.layoutIfNeeded()
//        return self.contentSize
//    }
//
//    override var contentSize: CGSize {
//        didSet{
//            self.invalidateIntrinsicContentSize()
//        }
//    }
    
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }


    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }

    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
    
    weak var relatedItemsDelegate: RelatedItemCellDelegate?

    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: .plain)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        self.delegate = self
        self.dataSource = self
        self.isScrollEnabled = false
        self.register(UINib.init(nibName: "RelatedItemsCell", bundle: nil), forCellReuseIdentifier: "RelatedItemsCell")
        self.backgroundColor = .clear
        self.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedItemsCell", for: indexPath) as! RelatedItemsCell
        let localItem = items[indexPath.row]
        cell.itemImage.image = UIImage.init(named: localItem.imageName)
//        if let image = localItem.image {
//            let url = URL(string: image)
//            cell.itemImage.kf.setImage(with: url)
//        }
        cell.itemName.text = localItem.name
        cell.itemOriginalPrice.attributedText = "Rs. \(localItem.originalPrice)".strikeThrough()
        cell.itemDiscountedPrice.text = "Rs. \(localItem.discountedPrice)"
        cell.itemQuantity.setTitle(localItem.quantity, for: .normal)
        cell.relatedDelegate = self
        cell.itemQuantity.tag = indexPath.row
        
//        cell.brandName.text = localItem.
//        if let assured = localItem.isAssured {
//            cell.assured.isHidden = !assured
//        } else {
//            cell.assured.isHidden = true
//        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 10)
        stackView.isLayoutMarginsRelativeArrangement = true
        
        for (index,text) in headerTitles.enumerated() {
            let label = UILabel()
            stackView.addArrangedAutolayoutSubview(label)
            apply(label) {
                $0.text = text
                if index == 0 {
                    $0.textColor = kDarkTextColor
                    $0.textAlignment = .left
                } else {
                    $0.textColor = kHeadingTitleTextColor
                    $0.textAlignment = .right
                }
                $0.font = UIFont(name: kMuliBold, size: 15)
            }
        }
        if let buttonTitle = self.headerButtonTitle {
            let contentView = UIView()
            
            let btn = UIButton()
            btn.setTitle(buttonTitle, for: .normal)
            btn.setTitleColor(UIColor(red: 255, green: 64, blue: 59), for: .normal)
            btn.titleLabel?.font = UIFont(name: kMuliBold, size: 14)
            btn.addTarget(self, action: #selector(self.headerButtonAction(sender:)), for: .touchUpInside)
            btn.contentHorizontalAlignment = .right
            
            let imageView = UIImageView(image: UIImage(named: "rightArrowRed"))
            contentView.addAutolayoutSubview(btn)
            contentView.addAutolayoutSubview(imageView)
            
            NSLayoutConstraint.activate([
                imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                imageView.widthAnchor.constraint(equalToConstant: 30),
                imageView.heightAnchor.constraint(equalToConstant: 30),
                
                btn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                btn.trailingAnchor.constraint(equalTo: imageView.leadingAnchor),
                btn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ])
            stackView.addArrangedAutolayoutSubview(contentView)
        }
        
        return stackView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func headerButtonAction(sender: UIButton){
        
    }
}

extension RelatedItemsTableView: RelatedItemCellDelegate {
    func selectquantity(sender: UIButton) {
        relatedItemsDelegate?.selectquantity(sender: sender)
    }
}

//
//  SelectQuantityViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 08/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
// MARK: - AddToCart
struct AddToCart: Codable {
    let code: Int?
    let response: String?
    let status, error: String?
}

class SelectQuantityViewModel{
    private var selectQuantities = [Quantity]()
    
    func getQuantityList(productId: String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        //let url = API.BASEURL+API.GETPRODUCTQUANTITY
        var url = URL(string: API.BASEURL+API.GETPRODUCTQUANTITY)!
        url.appendQueryItem(name: "productCode", value: productId)
        ServiceManager.shared.request(type: SelectQuantityModel.self, url: url.absoluteString, method: .get, controller: controller) { (result) in
            if let result = result, result.response != nil {
                print("Quantities result :\(result)")
                
                switch result.response! {
                case .string(let message):
                    completion(message)
                case .response(let response):
                    self.selectQuantities = response
                    completion(nil)
                }
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func addItemToCart(productCode: String, productQuantity: String, userId: String, controller: UIViewController){
        //{"productCode":"5","productQuantityCode":"0","userId":2}
        let paramDic = ["productCode":productCode,"productQuantityCode":productQuantity,"userId":userId]
        print("Add to Cart parameter :\(paramDic)")
        let url = API.BASEURL+API.ADDTOCART
        ServiceManager.shared.request(type: AddToCart.self, url: url, method: .post, view: controller.view, parameters: paramDic) { (result) in
            print("Add to Cart :\(String(describing: result))")
            if let result = result, result.code == 200 {
                controller.view.makeToast(result.response)
            } else if result?.error != nil {
                controller.view.makeToast(result?.error!)
            }
        }
    }
    
    func modelForQuantities()->[Quantity]{
        return self.selectQuantities
    }
}

//
//  QuantitySelectionModel.swift
//  Freshpick
//
//  Created by Ajeet N on 08/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//



import Foundation

// MARK: - QuantitySelectionModel
struct SelectQuantityModel: Codable {
    let code: Int?
    let response: SelectQuantity? //[Quantity]?
}

// MARK: - Response
struct Quantity: Codable {
    let id: Int?
    let productCode, quantity, savedAmount: String?
    let deleted: Bool?
    let actualAmount: Int?
    let finalAmount: DoublePrice?
    let createdAt, updatedAt: String?
}

enum SelectQuantity: Codable {
    case string(String)
    case response([Quantity])
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([Quantity].self) {
            self = .response(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(SelectQuantity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .response(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

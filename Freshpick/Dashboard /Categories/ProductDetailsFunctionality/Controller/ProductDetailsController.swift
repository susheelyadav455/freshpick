//
//  ProductDetailsController.swift
//  Freshpick
//
//  Created by Ajeet N on 09/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ProductDetailsController: UIViewController {
    var item: InItem?{
        didSet{
            self.updateModel()
        }
    }
    
    @IBOutlet var scrollContentView: UIView!
    private let detailsView = ProductDetailsView()
    let viewModel = SelectQuantityViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let productId = item?.id {
            self.viewModel.getQuantityList(productId: "\(productId)", controller: self) {(error) in
                if error != nil {
                    self.toast(msgString: error!, view: self.view)
                }
            }
        }
        
        
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = "Stawberry"
        
        let bagImage  = UIImage(named: "bag")
        let bagButton = UIBarButtonItem(image: bagImage,  style: .plain, target: self, action: #selector(didTapBagButton))
        bagButton.tintColor = .gray
        
        navigationItem.rightBarButtonItems = [bagButton]
        detailsView.quantityDelegate = self
        detailsView.stepperDelegate = self
        scrollContentView.addAutolayoutSubview(detailsView)

        let relatedItems = RelatedItemsTableView()
        relatedItems.relatedItemsDelegate = self
        relatedItems.headerTitles = ["Related Items"]
        relatedItems.items = [.init(imageName: "mangoes",
                                    name: "Seasoned Mangoes",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "oil_masala",
                                    name: "Oil & Masala",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "chicken",
                                    name: "Chicken",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "pedegree",
                                    name: "Pedigree",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149")
        ]
        scrollContentView.addAutolayoutSubview(relatedItems)
        
        NSLayoutConstraint.activate([
            detailsView.topAnchor.constraint(equalTo: scrollContentView.topAnchor, constant: 20),
            detailsView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor, constant: 20),
            detailsView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor, constant: -20),
        ])
        
        NSLayoutConstraint.activate([
            relatedItems.topAnchor.constraint(equalTo: detailsView.bottomAnchor, constant: 20),
            relatedItems.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor, constant: 12),
            relatedItems.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor, constant: -12),
            relatedItems.bottomAnchor.constraint(equalTo: scrollContentView.bottomAnchor, constant: -20),
        ])
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    //MARK:- Setup UI
    private func updateModel(){
        var itemQuantity = ""
        if let quantity = item?.quantity {
            switch quantity {
            case .integer(let val):
                itemQuantity = "\(val)"
            case .string(let val):
                itemQuantity = "\(val)"
            }
        }
        
        detailsView.model = ProductDetailsModel(images: [item?.image], manufacturer: item?.manufacturer, mrp: "\(item?.mrp ?? 0)", sellingPrice: "\(item?.sellingPrice?.price ?? 0.0)", quantity: itemQuantity, details: item?.details,name: item?.name)
    }
    
    //MARK: Shopping Bag Button Action
    @objc func didTapBagButton(sender: UIBarButtonItem){
        debugPrint("Shopping Button Tapped")
        guard let basketVC = self.storyboard?.instantiateViewController(identifier: "BasketVC") as? BasketVC else { return }
        basketVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(basketVC, animated: true)
    }
}

extension ProductDetailsController: RelatedItemCellDelegate{
    func selectquantity(sender: UIButton) {
        print("500g tapped :\(sender.tag)")
        if item?.id != nil {
            let controller = self.storyboard?.instantiateViewController(identifier: "PresentViewController") as! PresentViewController
            controller.modalPresentationStyle = .overCurrentContext
            // FIXME: Change for Related items Quantuty
            controller.items = self.viewModel.modelForQuantities()
            controller.presentDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension ProductDetailsController: PresentationDelegate {
    func selectedItem(item: Quantity) {
        print("Selected item :\(item)")
    }
    
    func proceedAction(sender: UIButton) {
        print("proceedAction :\(sender)")
    }
}

extension ProductDetailsController: QuantityDelegate{
    func quantitySelected(sender: UIButton) {
        if item?.id != nil {
            let controller = self.storyboard?.instantiateViewController(identifier: "PresentViewController") as! PresentViewController
            controller.modalPresentationStyle = .overCurrentContext
            controller.items = self.viewModel.modelForQuantities()
            controller.selectedItem = self.item?.id
            controller.presentDelegate = self
            controller.name = self.item?.name
            self.present(controller, animated: true, completion: nil)
        }        
    }
}

extension ProductDetailsController: StepperDelegate{
    func numberOfItems(count: Int) {
        print("Selected number of items are :\(count)")
        if let productQuantityId = item?.productQuantityCode {
            self.viewModel.addItemToCart(productCode: "\(item?.id ?? item?.productCode ?? 0)", productQuantity: "\(productQuantityId)", userId: ConstantValues.shared.userId, controller: self)
        } else {
            self.toast(msgString: "Invalid product quantity id for the item", view: view)
        }
    }
}

//
//  CategoryModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

// MARK: - CategoryModel
struct CategoryModel: Codable {
    let code: Int?
    let response: [Group]?
}

// MARK: - Response
struct Group: Codable {
    let id: Int?
    let name: String?
    let deleted: Bool?
    let createdAt, updatedAt: String?
    let subGroups: [InSubCategory]?
    let featureImage: String?
}

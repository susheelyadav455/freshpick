//
//  GroupViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit


class GroupViewModel {
    private var handler: ((_ error: String?)->Void)?
    private var groups = [Group]()
    
    func getGroups(controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        self.handler = completion
        let url = API.BASEURL+API.GETGROUPS
        ServiceManager.shared.request(type: CategoryModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result, result.response != nil {
                print("Group List result :\(result)")
                self.groups = result.response!
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func numberOfGroups()->Int{
        return self.groups.count
    }
    
    func subGroupForGroupAt(_ index:Int)->[InSubCategory]?{
        if self.groups.count>0 {
            return self.groups[index].subGroups
        }
        return nil
    }
    
    func getGroupsAsString()->[String]{
        let groupName = self.groups.map { $0.name }.compactMap { $0 }
        return groupName
    }
    //'[Group]
    func getGroups()->[Group]{
//        lself.groupset groupName = self.groups.map { $0.name }.compactMap { $0 }
        return self.groups
    }
}

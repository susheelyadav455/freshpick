//
//  CategoriesVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import UIKit

class CategoriesVC: ViewController {
    @IBOutlet weak var subCategoriesCollectionView: UICollectionView!
    @IBOutlet var categoryListContainerView: UIView!
    
    @IBOutlet var searchBar: UISearchBar!{
        didSet{
            searchBar.delegate = self
        }
    }
    
    let viewModel = GroupViewModel()
    private var selectedIndex = 0
    let groupCollectionView = FPThemeCollectionView(collectionViewLayout: UICollectionViewFlowLayout())

    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Categories"
        
        
        let notificationsImage    = UIImage(named: "notification")!
        let bagImage  = UIImage(named: "bag")!
        
        let notificationButton   = UIBarButtonItem(image: notificationsImage,  style: .plain, target: self, action: #selector(didTapNotificationButton))
        notificationButton.tintColor = .gray
        
        let bagButton = UIBarButtonItem(image: bagImage,  style: .plain, target: self, action: #selector(didTapBagButton))
        bagButton.tintColor = .gray
        navigationItem.rightBarButtonItems = [notificationButton, bagButton]
        
        self.categoryListContainerView.addAutolayoutSubview(groupCollectionView)
        groupCollectionView.fpThemeDelegate = self
        
        NSLayoutConstraint.activate([
            groupCollectionView.topAnchor.constraint(equalTo: self.categoryListContainerView.topAnchor),
            groupCollectionView.leadingAnchor.constraint(equalTo: self.categoryListContainerView.leadingAnchor),
            groupCollectionView.trailingAnchor.constraint(equalTo: self.categoryListContainerView.trailingAnchor),
            groupCollectionView.bottomAnchor.constraint(equalTo: self.categoryListContainerView.bottomAnchor)
        ])
    }
    
    //MARK: Notifications Button Action
    @objc func didTapNotificationButton(sender: UIBarButtonItem){
        debugPrint("Notification Button Tapped")
    }
    
    //MARK: Shopping Bag Button Action
    @objc  func didTapBagButton(sender: UIBarButtonItem){
        debugPrint("Shopping Button Tapped")
        
        guard let basketVC = self.storyboard?.instantiateViewController(identifier: "BasketVC") as? BasketVC else { return }
        basketVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(basketVC, animated: true)
        
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-ExtraBold", size: 32) ??
                    UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
        
        viewModel.getGroups(controller: self) {[weak self] (error) in
            if error == nil {
                let items = self?.viewModel.getGroupsAsString()
                if let groupItems = items {
                    self?.groupCollectionView.items = groupItems
                    self?.groupCollectionView.selectItem(at: IndexPath.init(row: self?.selectedIndex ?? 0, section: 0), animated: true, scrollPosition: .left)
                    self?.subCategoriesCollectionView.reloadData()
                }
            }
        }
    }
}

//MARK: UICollection View Delegates and DataSource Methods
extension CategoriesVC : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.subGroupForGroupAt(selectedIndex)?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoriesCell", for: indexPath) as! SubCategoriesCell
        
        let subGroups = self.viewModel.subGroupForGroupAt(selectedIndex)
        let subGroup = subGroups?[indexPath.row]
        
        cell.subCategoryName.text = subGroup?.name
        if let image = subGroup?.image{
            let url = URL(string: image)
            cell.SubCategoryImageView.kf.setImage(with: url)
        } else {
            cell.SubCategoryImageView.image = UIImage(named: "")
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let subGroups = self.viewModel.subGroupForGroupAt(selectedIndex)
        let subGroup = subGroups?[indexPath.row]
        let productListVC = self.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
        productListVC.navigationBarTitle = subGroup?.name
        productListVC.items = subGroup
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionWidth = subCategoriesCollectionView.bounds.width
        return CGSize(width: collectionWidth/3 - 2  , height: 175 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension CategoriesVC:UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //  Navigate to Search Controller
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        return false
    }
}

extension CategoriesVC: CellSelectionDelegate {
    func cellSelected(indexPath: IndexPath, item: AnyObject) {
        print("Selected Category Index :\(indexPath.row)")
        
        selectedIndex = indexPath.row
        self.subCategoriesCollectionView.reloadData()
    }
}

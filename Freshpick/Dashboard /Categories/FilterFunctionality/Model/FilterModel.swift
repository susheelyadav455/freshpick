//
//  FilterModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

// MARK: - BrandModel
struct BrandModel: Codable {
    let code: Int?
    let response: [Brand]?
}

// MARK: - Response
struct Brand: Codable {
    let id: Int?
    let name, classification, image, similar: String?
    let inactive: Bool?
    let margin: Double?
    let createdAt, updatedAt: String?
}

//enum AtedAt: String, Codable {
//    case the20200903T170012000Z = "2020-09-03T17:00:12.000Z"
//    case the20200907T141847000Z = "2020-09-07T14:18:47.000Z"
//}
//

//
//  PostFilterModel.swift
//  Freshpick
//
//  Created by Ajeet N on 10/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

// MARK: - PostFilterModel
struct PostFilterModel: Codable {
    let code: Int?
    let response: [InItem]?
}

//
//  FilterViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class FilterViewModel {
    private var brands = [Brand]()
    //private var items = [InItem]()
    
    func getBrands(controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        let url = API.BASEURL+API.GETBRANDS
        ServiceManager.shared.request(type: BrandModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result, result.response != nil {
                print("Brands result :\(result)")
                self.brands = result.response!
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func getBrands()->[Brand]{
        return self.brands
    }
    
    func postFilter(param: [String:Any], view:UIView,completion:@escaping ([InItem]?)->Void){
        let url = API.BASEURL+API.POSTFILTER
        ServiceManager.shared.request(type: PostFilterModel.self, url: url, method: .post, view: view, parameters: param) { (result) in
            if let result = result, result.response != nil {
                print("Post filter Brands result :\(result)")
                //self.items = result.response!
                completion(result.response!)
            } else {
                completion(nil)
            }
        }
    }
}

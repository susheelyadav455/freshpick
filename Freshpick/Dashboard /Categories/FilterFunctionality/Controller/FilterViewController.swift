//
//  FilterViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
import QuartzCore

struct FilterName {
    var filterName: String
    var filterItems: [FilterItem]
    var itemOpened: Bool
}

struct FilterItem {
    var filterItemName: String
    var selected = false
}

class FilterViewController: UIViewController {
    private let tableView = UITableView(frame: .zero, style: .plain)
    private var filters = [FilterName]()
    let viewModel = FilterViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.getBrands(controller: self) {[weak self] (error) in
            self?.filters.append(.init(filterName: "Brand", filterItems: (self?.viewModel.getBrands().compactMap({
                FilterItem(filterItemName: $0.name!)
            }))!, itemOpened: false))
            
            self?.filters.append(.init(filterName: "Price", filterItems: [
                .init(filterItemName: "0-100"),
                .init(filterItemName: "100-500")
            ], itemOpened: false))
            
            self?.filters.append(.init(filterName: "Discounts", filterItems: [
                .init(filterItemName: "10% off"),
                .init(filterItemName: "25% off"),
                .init(filterItemName: "30% off"),
                .init(filterItemName: "50% off"),
            ], itemOpened: false))
            self?.tableView.reloadData()
        }
        
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = "Filters"
        
        let shadowView = CustomView()
        shadowView.setDefaultShadow()
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(shadowView)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.layer.cornerRadius = 10
        tableView.allowsMultipleSelection = true
        tableView.clipsToBounds = true
        tableView.register(UINib.init(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        tableView.register(UINib.init(nibName: "FilterHeaderCell", bundle: nil), forCellReuseIdentifier: "FilterHeaderCell")
        tableView.register(UINib.init(nibName: "FilterHeaderButtonCell", bundle: nil), forCellReuseIdentifier: "FilterHeaderButtonCell")

        shadowView.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            shadowView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 14),
            shadowView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 14),
            shadowView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -14),
            shadowView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -14)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: shadowView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor, constant: 16),
            tableView.trailingAnchor.constraint(equalTo: shadowView.trailingAnchor, constant: -16),
            tableView.bottomAnchor.constraint(equalTo: shadowView.bottomAnchor, constant: -16),
        ])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        if let view = sender?.view {
            filters[view.tag].itemOpened.toggle()
            self.tableView.reloadData()
        }
    }
    
    @objc private func applyButtonAction(sender: UIButton){
        print("Selected rows of tableview :\(String(describing: self.tableView.indexPathsForSelectedRows))")
        if let selectedIndexPath = self.tableView.indexPathsForSelectedRows {
            var brandArray = [String]()
            var discountArray = [[String:Any]]()
            var finalPriceArray = [[String:Any]]()

            for path in selectedIndexPath {
                if path.section==0 {
                    //  Brand
                    let filter = self.filters[path.section]
                    brandArray.append(filter.filterItems[path.row].filterItemName)
                } else if path.section==1 {
                    // price
                    let price = self.filters[path.section]
                    let priceArray = price.filterItems[path.row].filterItemName.components(separatedBy: "-").compactMap { Int($0) }
                    let priceDict = ["lower":"\(priceArray.first ?? 0)","upper":"\(priceArray.last ?? 0)"]
                    finalPriceArray.append(priceDict)
                } else if path.section==2 {
                    // discount
                    
                    let price = self.filters[path.section]
                    let priceArray = price.filterItems[path.row].filterItemName.components(separatedBy: "%").compactMap { Int($0) }

                    if discountArray.count>0 {
                        let previousValue = discountArray[0]
                        let previousLowerValue = previousValue["lower"] as? Int //Int(previousValue["lower"] as? String ?? "")
                        let previousUpperValue = previousValue["upper"] as? Int //Int(previousValue["upper"] as? String ?? "")

                        let minimumValuePresent = min(priceArray.first ?? 0, priceArray.last ?? 0)
                        let minimumValuePast = min(previousLowerValue ?? 0, previousUpperValue ?? 0)
                        let finalMinimumValue = min(minimumValuePresent, minimumValuePast)

                        let maxValuePresent = max(priceArray.first ?? 0, priceArray.last ?? 0)
                        let maxValuePast = max(previousLowerValue ?? 0, previousUpperValue ?? 0)
                        let finalMaxValue = max(maxValuePresent, maxValuePast)

                        let priceDict = ["lower":finalMinimumValue,"upper":finalMaxValue]
                        discountArray.removeAll()
                        discountArray.append(priceDict)
                    } else {
                        var priceDic = [String:Any]()
                        if priceArray.count>1 {
                            priceDic = ["lower":priceArray.first ?? 0,"upper":priceArray.last ?? 0]
                        } else {
                            priceDic = ["lower":0,"upper":priceArray.last ?? 0]
                        }
                        //let priceDict =
                        discountArray.removeAll()
                        discountArray.append(priceDic)
                    }
                }
            }

            let filterDic = ["brand":brandArray,"discount":discountArray,"price":finalPriceArray] as [String : Any]
            print("Filter Dictionary is :\(filterDic)")
            self.viewModel.postFilter(param: filterDic, view: view) {[weak self] (result) in
                if let items = result {
                    self?.toast(msgString: "Filter Success", view: self?.view ?? UIView())
                    //  Navigate to Product List
                    let productListVC = self?.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
                    productListVC.itemList = items
                    productListVC.callAPI = false
                    self?.navigationController?.pushViewController(productListVC, animated: true)
                } else {
                    self?.toast(msgString: "Unable to apply filter", view: self?.view ?? UIView())
                }
            }
        }
    }
    
    private func toggleSelection(_ indexPath: IndexPath, _ selection: Bool){
        filters[indexPath.section].filterItems[indexPath.row].selected = selection
        self.tableView.reloadData()
    }
}

extension FilterViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < filters.count {
            let items = filters[section]
            if items.itemOpened {
                return items.filterItems.count
            } else {
                return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section < filters.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
            let filter = filters[indexPath.section].filterItems[indexPath.row]
            //cell.filterItem = filter
            cell.filterTitle.text = filter.filterItemName
            
            if filter.selected {
                cell.filterTitle.textColor = UIColor(red: 39, green: 45, blue: 47)
                cell.filterTitle.font = UIFont(name: "Muli-Bold", size: 15)
                cell.checkMArkButton.isSelected = true
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            } else {
                cell.filterTitle.textColor = UIColor(red: 181, green: 181, blue: 181)
                cell.filterTitle.font = UIFont(name: "Muli-Bold", size: 15)
                cell.checkMArkButton.isSelected = false
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section < filters.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell") as! FilterHeaderCell
            cell.headerTitle.text = filters[section].filterName
            cell.tag = section
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            cell.addGestureRecognizer(tap)
            cell.isUserInteractionEnabled = true
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderButtonCell") as! FilterHeaderButtonCell
            cell.backgroundColor = .white
            cell.applyButton.addTarget(self, action: #selector(self.applyButtonAction(sender:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section < filters.count {
            return 55
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == 2 {
//            tableView.allowsMultipleSelection = false
//        } else {
//            tableView.allowsMultipleSelection = true
//        }
        
        if let _ = tableView.cellForRow(at: indexPath) as? FilterCell {
            toggleSelection(indexPath, true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let _ = tableView.cellForRow(at: indexPath) as? FilterCell {
            toggleSelection(indexPath, false)
        }
    }
}

//
//  FilterCell.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {
    @IBOutlet var checkMArkButton: UIButton!
    
    @IBOutlet var filterTitle: UILabel!
//    var filterItem: FilterItem? {
//        didSet{
//            applyModel()
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor(red: 250, green: 250, blue: 250)
        self.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
//            filterTitle.textColor = UIColor(red: 39, green: 45, blue: 47)
//            filterTitle.font = UIFont(name: "Muli-Bold", size: 15)
//            checkMArkButton.isSelected = true
//            filterItem?.selected = true
        } else {
//            filterTitle.textColor = UIColor(red: 181, green: 181, blue: 181)
//            filterTitle.font = UIFont(name: "Muli-Bold", size: 15)
//            checkMArkButton.isSelected = false
//            filterItem?.selected = false
        }
    }
    
//    private func applyModel(){
//        filterTitle.text = filterItem?.filterItemName
//    }
}

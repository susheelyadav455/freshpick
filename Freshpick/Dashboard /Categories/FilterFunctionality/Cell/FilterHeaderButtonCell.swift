//
//  FilterHeaderButtonCell.swift
//  Freshpick
//
//  Created by Ajeet N on 08/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class FilterHeaderButtonCell: UITableViewCell {
    @IBOutlet var applyButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        applyButton.layer.cornerRadius = 10
        self.backgroundColor = .white
        self.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

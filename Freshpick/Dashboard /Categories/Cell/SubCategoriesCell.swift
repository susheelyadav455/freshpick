//
//  SubCategoriesCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 24/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SubCategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var subCategoryBackgroundView: CustomView!
    @IBOutlet weak var SubCategoryImageView: UIImageView!
    @IBOutlet weak var subCategoryName: UILabel!
    @IBOutlet weak var subCategoryDiscount: UILabel!
    
    
}

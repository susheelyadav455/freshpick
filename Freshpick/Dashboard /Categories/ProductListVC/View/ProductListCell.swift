//
//  ProductListCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 22/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {

    @IBOutlet weak var customBackgroundView: CustomView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productQuantity: UIButton!
    @IBOutlet weak var addQuantityOutlet: UIButton!
    @IBOutlet weak var productFinalPrice: UILabel!
    @IBOutlet weak var productEstimatedPrice: UILabel!
    
    var imageData: ProductStaticImages? {
        didSet{
            self.productImageView.image = UIImage(named: self.imageData?.Pimage ?? "")
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

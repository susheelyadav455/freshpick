//
//  ProductListViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 06/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ProductListViewModel {
    private var handler: ((_ error: String?)->Void)?
    private var itemsObjects = [InItem]()
    
    func getListOfProducts(withId:String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        self.handler = completion
        let url = API.BASEURL+API.PRODUCT+"?id=\(withId)"
        ServiceManager.shared.request(type: ProductListModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result {
                print("Product List result :\(result)")
                if let items = result.response {
                    switch items {
                    case .string(let str):
                        completion(str)
                    case .items(let item):
                        self.itemsObjects = item
                        completion(nil)
                    }
                }
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func setItems(items:[InItem]){
        self.itemsObjects = items
    }
    
    func numberOfRowsForSection(_ at:Int)->Int{
        return self.itemsObjects.count
    }
    
    func modelForRow(_ at:Int)->InItem{
        return self.itemsObjects[at]
    }
}

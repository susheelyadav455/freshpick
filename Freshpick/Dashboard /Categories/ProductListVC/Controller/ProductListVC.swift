//
//  ProductListVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 22/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
import Kingfisher

struct  ProductStaticImages {
    let Pimage : String
    init(Pimage: String) {
        self.Pimage = Pimage
    }
}

class ProductListVC: UIViewController {
    var items: InSubCategory?
    
    @IBOutlet weak var productTableViewObj: UITableView! {
        didSet{
            self.productTableViewObj.delegate = self
            self.productTableViewObj.dataSource = self
            self.productTableViewObj.isScrollEnabled = false
            self.productTableViewObj.register(UINib.init(nibName: "PastOrderCell", bundle: nil), forCellReuseIdentifier: "PastOrderCell")
        }
    }
    
    @IBOutlet var scrollContentViewObj: UIView!
    @IBOutlet var tableViewTopAnchor: NSLayoutConstraint!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    private var productImages = [ProductStaticImages]()
    private let searchBar:FPSearchBar = .fromNib()
    let viewModel = ProductListViewModel()
    let selectQuantityViewModel = SelectQuantityViewModel()
    var callAPI = true
    var itemList:[InItem]?
    var navigationBarTitle: String?
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollContentViewObj.addAutolayoutSubview(searchBar)

        apply(searchBar) {
            $0.fpSearchBar = self
            $0.setShadow(shadowRadius: 5)
            $0.navigateOnBecomeFirstResponder = true
        }
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: scrollContentViewObj.topAnchor, constant: 16),
            searchBar.leadingAnchor.constraint(equalTo: scrollContentViewObj.leadingAnchor, constant: 16),
            searchBar.trailingAnchor.constraint(equalTo: scrollContentViewObj.trailingAnchor, constant: -16),
            searchBar.heightAnchor.constraint(equalToConstant: 40),
            productTableViewObj.topAnchor.constraint(equalTo: searchBar.bottomAnchor,constant: 10)
        ])
        
        if callAPI {
            viewModel.getListOfProducts(withId: "\(items?.id ?? 0)", controller: self) {[weak self] (error) in
                guard let self = self else { return }
                if error == nil {
                    self.productTableViewObj.reloadData()
                    self.view.layoutIfNeeded()
                    print("Y axis for API call is : \(self.productTableViewObj.frame.origin.y)")
                    self.tableViewHeight.constant = self.productTableViewObj.contentSize.height+self.productTableViewObj.frame.origin.y
                } else {
                    let empView = self.view.addEmptyState(type: .noProductsFound(error ?? "", UIImage(named: "myOrdersEmptyState")!, nil)) as! EmptyStateView
                    empView.delegate = self
                }
            }
        } else {
            viewModel.setItems(items: itemList!)
            if itemList?.count ?? 0 > 0 {
                self.productTableViewObj.reloadData()
                itemList!.forEach { _ in
                    self.view.layoutIfNeeded()
                    self.tableViewHeight.constant = self.productTableViewObj.contentSize.height
                }
            } else {
                let empView = self.view.addEmptyState(type: .noProductsFound("No proucts found!!!", UIImage(named: "myOrdersEmptyState")!, nil)) as! EmptyStateView
                empView.delegate = self
            }
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = self.navigationBarTitle
        
        productImages = [ProductStaticImages(Pimage: "straw") , ProductStaticImages(Pimage: "grapes"), ProductStaticImages(Pimage: "apples"), ProductStaticImages(Pimage: "straw"), ProductStaticImages(Pimage: "grapes"), ProductStaticImages(Pimage: "apples"), ProductStaticImages(Pimage: "mangoes"), ProductStaticImages(Pimage: "straw")]
                
        let filterImage    = UIImage(named: "filter")
        let bagImage  = UIImage(named: "bag")
        
        let filterButton  = UIBarButtonItem(image: filterImage,  style: .plain, target: self, action: #selector(didTapFilterButton))
        filterButton.tintColor = .gray
        
        let bagButton = UIBarButtonItem(image: bagImage,  style: .plain, target: self, action: #selector(didTapBagButton))
        bagButton.tintColor = .gray
        
        navigationItem.rightBarButtonItems = [filterButton, bagButton]
    }
    
    //MARK: Filter Button Action
    @objc func didTapFilterButton(sender: UIBarButtonItem){
        debugPrint("Filter Button Tapped")
        let filterObj = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(filterObj, animated: true)
        
    }
    
    
    //MARK: Shopping Bag Button Action
    @objc func didTapBagButton(sender: UIBarButtonItem){
        debugPrint("Shopping Button Tapped")
        
        guard let basketVC = self.storyboard?.instantiateViewController(identifier: "BasketVC") as? BasketVC else { return }
        basketVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(basketVC, animated: true)
        
    }
    
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-ExtraBold", size: 32) ??
                    UIFont.systemFont(ofSize: 30)]
            navigationController?.navigationBar.prefersLargeTitles = true
            

            // Or, Set to new colour for just this navigation bar
//            self.navigationController?.navigationBar.barTintColor = UIColor.blue
            
//            navigationController?.navigationBar.barTintColor = UIColor.green
//            UINavigationBar.appearance().barTintColor = .green
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func quantityButtonSelected(sender: UIButton) {
        let item = self.viewModel.modelForRow(sender.tag)
        self.selectQuantityViewModel.getQuantityList(productId: "\(item.id ?? 0)", controller: self) {(error) in
            if error == nil {
                let controller = self.storyboard?.instantiateViewController(identifier: "PresentViewController") as! PresentViewController
                controller.modalPresentationStyle = .overCurrentContext
                controller.items = self.selectQuantityViewModel.modelForQuantities()
                controller.selectedItem = item.id
                controller.presentDelegate = self
                controller.name = item.name
                self.present(controller, animated: true, completion: nil)
            } else {
                self.toast(msgString: error!, view: self.view)
            }
        }
    }
}


//MARK: UITableView Delegate and DataSource Methods
extension ProductListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PastOrderCell", for: indexPath) as? PastOrderCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        let item = self.viewModel.modelForRow(indexPath.row)
        cell.brandName.text = item.manufacturer
        cell.itemName.text = item.name
        cell.itemOriginalPrice.attributedText = "Rs.\(item.mrp ?? 0)".strikeThrough()
        cell.itemDiscountedPrice.text = "Rs.\(item.sellingPrice?.price ?? 0.0)"
        
        var itemQuantity = ""
        if let quantity = item.quantity {
            switch quantity {
            case .integer(let val):
                itemQuantity = "\(val)"
            case .string(let val):
                itemQuantity = "\(val)"
            }
        }
        
        cell.itemQuantity.setTitle(itemQuantity, for: .normal)
        if let image = item.image{
            let url = URL(string: image)
            cell.itemImage.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"))
        } else {
            cell.itemImage.image = UIImage(named: self.productImages[0].Pimage)
        }
        cell.itemQuantity.tag = indexPath.row
        cell.itemQuantity.addTarget(self, action: #selector(self.quantityButtonSelected(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        vc.item = self.viewModel.modelForRow(indexPath.row)
        navigationController?.pushViewController(vc, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 148
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // 1. Set the initial state of the cell
              cell.alpha = 0
        
        // 2. UIView animation Method to change to final state of the cell
              UIView.animate(withDuration: 1.0) {
              cell.alpha = 1.0
        }
    }
    
}

extension ProductListVC: PresentationDelegate {
    func selectedItem(item: Quantity) {
        print("Selected item :\(item)")
    }
    
    func proceedAction(sender: UIButton) {
        print("proceedAction :\(sender)")
    }
}

extension ProductListVC: FPSearchBarDelegate{
    func searchBarShouldBeginEditing(_ textField: UITextField, _ shouldEdit: Bool) {
        if !shouldEdit {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func searchButtonClicked(_ text: String) {
        print("HomeVC Search Button clicked :\(text)")
    }
    
    func searchBarText(_ text: String) {
        print("HomeVC Search text :\(text)")
    }
}

extension ProductListVC: EmptyStateProtocol {
    func buttonAction(sender: UIButton) {
        print("tappedddddd....=====...")
    }
    
    
}

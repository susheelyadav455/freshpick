//
//  ProductListModel.swift
//  Freshpick
//
//  Created by Ajeet N on 06/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

// MARK: - SearchModel
struct ProductListModel: Codable {
    let code: Int?
    let response: InItemResponse?
}

enum InItemResponse: Codable {
    case string(String)
    case items([InItem])
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([InItem].self) {
            self = .items(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Quantity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .items(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

//
//  AllCategoriesViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 22/11/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class AllCategoriesViewController: UIViewController {
    var itemList: [CategoryItems]?
    var navigationBarTitle: String?
    var groupViewModel: GroupViewModel?
    private var allCategory: CategoryView!
    var viewType: CategoryType?
    var filterViewModel: FilterViewModel?
    
    @IBOutlet var scrollContentViewObj: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        scrollContentViewObj.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        if let viewType = self.viewType {
            allCategory = CategoryView(delegate: self, productCategory: viewType)
        } else {
            allCategory = CategoryView(delegate: self, productCategory: CategoryType.category)
        }
        
        addSubViews()
        applyModel()
        addAutoLayoutToSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = self.navigationBarTitle
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func addSubViews() {
        scrollContentViewObj.addAutolayoutSubview(allCategory)
    }
    
    private func applyModel() {
        apply(allCategory) {
            $0.numberOfColumns = 2
            $0.cellHeight = 173
            $0.isOfferViewHidden = true
            if let items = self.itemList {
                $0.items = items
            }
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        }
    }
    
    private func addAutoLayoutToSubviews() {
        NSLayoutConstraint.activate([
            allCategory.topAnchor.constraint(equalTo: scrollContentViewObj.topAnchor, constant: 20),
            allCategory.leadingAnchor.constraint(equalTo: scrollContentViewObj.leadingAnchor, constant: 16),
            allCategory.trailingAnchor.constraint(equalTo: scrollContentViewObj.trailingAnchor, constant: -16),
            allCategory.bottomAnchor.constraint(equalTo: scrollContentViewObj.bottomAnchor, constant: -20)
        ])
    }
}

extension AllCategoriesViewController: CategoryCollectionViewSelection {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType) {
        
        
        switch productCategory {
        case .brands:
            print("AllCategoriesViewController brands called")
            
            // Call Brands API call
            guard filterViewModel != nil else { return }
            let filterDic = ["brand":["\(item.name!)"], "discount": [], "price": []] as [String : Any]
            print("Filter Dictionary is :\(filterDic)")
            self.filterViewModel!.postFilter(param: filterDic, view: view) {[weak self] (result) in
                if let items = result {
                    //  Navigate to Product List
                    let productListVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(identifier: "ProductListVC") as! ProductListVC
                    productListVC.itemList = items
                    productListVC.callAPI = false
                    productListVC.navigationBarTitle = item.name
                    self?.navigationController?.pushViewController(productListVC, animated: true)
                } else {
                    self?.toast(msgString: "\(item.name!) has no products", view: self?.view ?? UIView())
                }
            }
            
            break
        case .category:
            let pdVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SubCategoryListViewController") as! SubCategoryListViewController
            pdVC.groupViewModel = self.groupViewModel
            pdVC.subcategoryIndex = index
            pdVC.navigationBarTitle = self.groupViewModel?.getGroups()[index].name
            self.navigationController?.pushViewController(pdVC, animated: true)
            break
        case .search:
            break
        case .offers:
            break
        }
    }
}

//
//  OrderDetailsVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 08/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
import QuartzCore


class OrderDetailsVC: UIViewController {
    
    
    @IBOutlet weak var ordersTableView: UITableView! {
        didSet{
            self.ordersTableView.delegate = self
            self.ordersTableView.dataSource = self
            self.ordersTableView.tableFooterView =  UIView()
        }
    }
    

    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "My Orders"
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 25) ??
                                         UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
    }
    
}


//MARK: UITableView Delegate and Datasource Methods
extension OrderDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfOrdersCell", for: indexPath) as? ListOfOrdersCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.orderStatusLable.layer.cornerRadius = 10
        cell.orderStatusLable.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = OrderDetailsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

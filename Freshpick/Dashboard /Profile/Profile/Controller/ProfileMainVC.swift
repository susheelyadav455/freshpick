//
//  ProfileMainVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import UIKit


//MARK: First TableView Model Data
struct ProfileFirstTableViewData {
    let title: String
    let imageName: String
    let subTitle: String
    init(title:String,imageName:String,subTitle:String) {
           self.title = title
           self.imageName = imageName
           self.subTitle = subTitle
       }
}

//MARK: Second TableView Model Data
struct ProfileSecondTableViewData {
    let titleSecond: String
    let imaegNameSecond: String
    init(titleSecond: String, imaegNameSecond: String){
        self.titleSecond = titleSecond
        self.imaegNameSecond = imaegNameSecond
    }
}


class ProfileMainVC: ViewController {

    private var profileFirstTableViewData = [ProfileFirstTableViewData]()
    private var profileSecondTableViewData = [ProfileSecondTableViewData]()
    
    @IBOutlet weak var secondCustomView: CustomView!
    @IBOutlet weak var firstTableView: UITableView! {
        didSet {
            self.firstTableView.delegate = self
            self.firstTableView.dataSource = self
            self.firstTableView.tableFooterView =  UIView()
        }
    }
    
    @IBOutlet weak var secondTableView: UITableView! {
        didSet {
            self.secondTableView.delegate = self
            self.secondTableView.dataSource = self
            self.secondCustomView.layer.cornerRadius = 15
            self.secondTableView.tableFooterView = UIView()
            
        }
    }
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()


    profileFirstTableViewData = [ProfileFirstTableViewData(title: "My Addresses", imageName: "home", subTitle: "Add and edit address"), ProfileFirstTableViewData(title: "Order Details", imageName: "orderdetails", subTitle: "Track Order, Past Order"), ProfileFirstTableViewData(title: "Offer Zone", imageName: "offers", subTitle: "Grab awesome deals and offers"), ProfileFirstTableViewData(title: "Notifications", imageName: "notification", subTitle: "Stay Updated with FreshPicks"), ProfileFirstTableViewData(title: "FreshPicks Wallet", imageName: "wallet", subTitle: "Easy to use, for faster checkout"), ProfileFirstTableViewData(title: "FreshPicks Membership", imageName: "membership", subTitle: "Exclusive discounts for premium members")]
        
        
    profileSecondTableViewData = [ProfileSecondTableViewData(titleSecond: "Cookbook", imaegNameSecond: "cookbook"), ProfileSecondTableViewData(titleSecond: "FreshPick Assured", imaegNameSecond: "assured"), ProfileSecondTableViewData(titleSecond: "Partnership with Indian Farmers", imaegNameSecond: "partnership")]
        
    }
    
    
//MARK: View Did Disappear
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: Profile Edit Button Action
    @IBAction func profileEditButtonAction(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
        guard let profileEditVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC else { return }
        profileEditVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileEditVC, animated: true)
         }
    }
    
    
    //MARK: Logout Button Action
    @IBAction func logoutButtonAction(_ sender: Any) {
        
        self.alert(title: "Are you sure?", message: "Are you sure you want to Logout from the app ?", actionTitles: ["Cancel","Logout"], actionStyle: [.default,.default], action: [
                   { Cancel in
                       
                   },{ Logout in
                    self.navigationController?.popToRootViewController(animated: true)
                   }
               ])
    }
}

//MARK: UITableView Delegate and Datasource Methods
extension ProfileMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case firstTableView:
            return profileFirstTableViewData.count
        case secondTableView:
            return profileSecondTableViewData.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case firstTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileFirstCell", for: indexPath) as? ProfileFirstCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.FirstData = self.profileFirstTableViewData[indexPath.row]
            return cell
        case secondTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSecondCell", for: indexPath) as? ProfileSecondCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.secondData = self.profileSecondTableViewData[indexPath.row]
            return cell
            
        default:
            return UITableViewCell()
        }
        
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case firstTableView:
             return 80
        case secondTableView:
             return 60
        default:
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tableView {
        case firstTableView:
            
        if indexPath.row == 0 {

            DispatchQueue.main.asyncAfter(deadline: .now()) {
                  guard let myAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "ListOfAddressVC") as? ListOfAddressVC else { return }
            myAddressVC.hidesBottomBarWhenPushed = true
              self.navigationController?.pushViewController(myAddressVC, animated: false)
              }
          }
  
        else if indexPath.row == 1{
            
            DispatchQueue.main.asyncAfter(deadline: .now()) {
             guard let orderDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as? OrderDetailsVC else { return }
                orderDetailsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(orderDetailsVC, animated: false)
                   }
            }
            
            else if indexPath.row == 2 {
                print("offer zone")
            }
                
            else if indexPath.row == 3 {
                print("notifications")
            }
            
            else if indexPath.row == 4 {
                print("fresh picks wallet")
            }
            else if indexPath.row == 5 {
                print("membership")
            
            }
        
                
          case secondTableView:
            if indexPath.row == 0 {
                print("cookbook ")
                
            }
            else if indexPath.row == 1 {
                print("fresh pick assured")
            }
            else if indexPath.row == 2 {
                print("partneship ")
            }
            
            default:
            break
            
    }
    
    }
        
}

//
//  EditProfileVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 08/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit


class EditProfileVC: UIViewController {
    
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    
    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 18)!]
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Edit Account"
        
        
        let leftBarButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: self, action: #selector(myLeftSideBarButtonItemTapped(_:)))
        leftBarButton.tintColor = UIColor.orange
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        
        let rightBarButton = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.done, target: self, action: #selector(myRightSideBarButtonItemTapped(_:)))
        rightBarButton.tintColor = UIColor.orange
        self.navigationItem.rightBarButtonItem = rightBarButton

    }
    
    
   //MARK: Save Button Action
    @objc func myRightSideBarButtonItemTapped(_ sender:UIBarButtonItem!) {
        
    }
    
    
    //MARK: Cancel Button Action
    @objc func myLeftSideBarButtonItemTapped (_ sender:UIBarButtonItem!) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 25) ??
                                         UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
        
    
    }

    
}

//
//  AddAddressVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 09/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {
    
    
    @IBOutlet weak var houseNumberTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var landMarkTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var pinCodetextField: UITextField!
    
    
    let viewModel = AddressViewModel()
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 18)!]
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Add Address"
        
        
        let leftBarButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: self, action: #selector(myLeftSideBarButtonItemTapped(_:)))
        leftBarButton.tintColor = UIColor.orange
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        
        let rightBarButton = UIBarButtonItem(title: "Locate me", style: UIBarButtonItem.Style.done, target: self, action: #selector(myRightSideBarButtonItemTapped(_:)))
        rightBarButton.tintColor = UIColor.orange
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    //MARK: Location Button Action
    @objc func myRightSideBarButtonItemTapped(_ sender:UIBarButtonItem!) {
        
    }
    
    
    //MARK: Cancel Button Action
    @objc func myLeftSideBarButtonItemTapped (_ sender:UIBarButtonItem!) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: Save Address
    @IBAction func saveAddress(_ sender: Any) {
        
        let houseNumber = self.houseNumberTextField.text ?? ""
        let landMark = self.landMarkTextField.text ?? ""
        let locality = self.streetTextField.text ?? ""
        let pincode = self.pinCodetextField.text ?? ""
        let city = self.cityTextField.text ?? ""
        let params = ["houseNo": houseNumber, "locality":locality, "landmark":landMark, "pincode":pincode, "address_type": "Home", "default":"0", "city":city]
        
        viewModel.postUserAddress(withID: "1", parameters: params, controller: self) { (error) in
            debugPrint("The result of post address is :\(String(describing: error)) ")

            if error == nil {
                // Success
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        
        }
        
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 25) ??
                    UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
        
        
    }
}

//
//  ListOfAddressVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 09/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
import QuartzCore


class ListOfAddressVC: UIViewController {
    
    @IBOutlet weak var customBackgrouondView: CustomView!
    @IBOutlet weak var AddressTableView: UITableView!{
        didSet{
            self.AddressTableView.delegate = self
            self.AddressTableView.dataSource = self
            self.AddressTableView.tableFooterView =  UIView()
            
        }
    }
    let viewModel = AddressViewModel()
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "My Address"
    }
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 25) ??
                    UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
        
        viewModel.getAddressList(withId: "2", controller: self) {[weak self] (result) in
            debugPrint("The address result is :\(String(describing: result))")
            if result == nil {
                self?.AddressTableView.reloadData()
            }
            else {
                self?.toast(msgString: result ?? "", view: self?.view ?? UIView())
            }
        }
    }
    
    // MARK: Delete Address Button Action
    @objc func deleteButtonAction(sender: UIButton){
        self.alert(title: "Are you sure?", message: "Are you sure you want to delete the item", actionTitles: ["Cancel","Delete"], actionStyle: [.default,.default], action: [
            { Cancel in
                
            },{ Delete in
                
                let addressId = ["id": 1]
                self.viewModel.deleteUserAddress(withID: "2", parameters: addressId, controller: self) { (result) in
                    print("The result of delete address type is :\(String(describing: result))")
                    if result == nil {
                        
                    }
                }
            }
        ])
        
    }
    
    //MARK: Add Address Button Action
    @IBAction func addAddressButtonAction(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            guard let addAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as? AddAddressVC else { return }
            self.navigationController?.pushViewController(addAddressVC, animated: false)
        }
    }
    
}

//MARK: UITableViewDelegate and UITableView DataSource Methods
extension ListOfAddressVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfAddresCell", for: indexPath) as? ListOfAddresCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        let address = viewModel.modelForRow(indexPath.row)
        var addressString = ""
        
        if let houseNo = address.houseNo {
            addressString.append(houseNo)
        }
        
        if let locality = address.locality {
            addressString.append(", \(locality)")
        }
        
        if let landmark = address.landmark {
            addressString.append(", \(landmark)")
        }
        
        if let city = address.city {
            addressString.append(", \(city)")
        }
        
        if let pincode = address.pincode {
            addressString.append(", \(pincode)")
        }
        
        cell.addressLabel.text = addressString
        cell.addressStatusLabel.text = address.addressType
        
        //        {
        //            "id": 2,
        //            "user_id": "2",
        //            "houseNo": "90-986201",
        //            "locality": "Flat 801,Sresidency, Miyapur",
        //            "landmark": "Asian Gpr Opp",
        //            "pincode": "5966630",
        //            "address_type": "Work",
        //            "default": true,
        //            "deleted": false,
        //            "city": "Hyderabad",
        //            "createdAt": "2020-09-04T12:31:17.000Z",
        //            "updatedAt": "2020-09-04T12:31:17.000Z"
        //        }
        
        cell.deleteAddrressButton.tag = indexPath.row
        cell.deleteAddrressButton.addTarget(self, action: #selector(self.deleteButtonAction(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}

//
//  ListOfAddresCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 09/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ListOfAddresCell: UITableViewCell {

    
    @IBOutlet weak var deleteAddrressButton: UIButton!
    @IBOutlet weak var editAddressButton: UIButton!
    @IBOutlet weak var addressStatusImage: UIImageView!
    @IBOutlet weak var addressStatusLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

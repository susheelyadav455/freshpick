//
//  DeleteModel.swift
//  Freshpick
//
//  Created by susheel chennaboina on 12/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation


struct DeleteModel: Codable {
    let code: Int?
    let response: String?
    let error: String?
}

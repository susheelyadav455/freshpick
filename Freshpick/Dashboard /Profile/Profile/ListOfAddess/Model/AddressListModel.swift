//
//  AddressListModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

// MARK: - AddressListModel
struct AddressListModel: Codable {
    let code: Int?
    let response: [Address]?
    let error : String?
}

// MARK: - Response
struct Address: Codable {
    let id: Int?
    let userID, houseNo, locality, landmark: String?
    let pincode, addressType: String?
    let responseDefault, deleted: Bool?
    let city, createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case houseNo, locality, landmark, pincode
        case addressType = "address_type"
        case responseDefault = "default"
        case deleted, city, createdAt, updatedAt
    }
}

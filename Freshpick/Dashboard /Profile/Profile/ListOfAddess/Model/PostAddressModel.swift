//
//  PostAddressModel.swift
//  Freshpick
//
//  Created by susheel chennaboina on 12/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation


struct PostAddressModel: Codable {
    let code: Int?
    let response: AddressResponse?
    let error: String?
}

struct AddressResponse: Codable {
    let user_id : Int?
    let houseNo : String?
    let locality : String?
    let landmark : String?
    let pincode : String?
    let address_type : String?
    let city: String?
}


//
//  AddressViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit


class AddressViewModel {
    //private var handler: ((_ error: String?)->Void)?
    private var addressList = [Address]()
//    private var UserAddressPostedList = AddressResponse()

    
    func getAddressList(withId:String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        //self.handler = completion
        let url = API.BASEURL+API.ADDRESS+"?user_id=\(withId)"   //  ?user_id=2
        ServiceManager.shared.request(type: AddressListModel.self, url: url, method: .get, controller: controller) { (result) in
            
            if let result = result, result.code == 200 {
                print("Address List result :\(result)")
                self.addressList = result.response!
                completion(nil)
            } else {
                completion(result?.error ?? "")
            }
        }
    }
    

    //MARK: Delete User Address.
    func deleteUserAddress(withID:String, parameters: [String: Any], controller:UIViewController, completion:@escaping (_ error: String?)->Void) {
        let deleteUrl = API.BASEURL + API.ADDRESS + "?user_id=\(withID)"
        ServiceManager.shared.request(type: DeleteModel.self, url: deleteUrl, method: .delete, view: controller.view, parameters: parameters) { (result) in
            print("The result of delete address in view model is  :\(String(describing: result))")
            if let result = result, result.code == 200 {
                print("The response of delete address is : \(String(describing: result))")
                completion(nil)
            }
            else {
                completion(result?.error ?? "")
            }
        }
        
    }
    
    //MARK: Post Address
    func postUserAddress(withID: String, parameters: [String: Any], controller: UIViewController, completion:@escaping (_ error: String?)-> Void) {
        
        let postAddressUrl = API.BASEURL + API.ADDRESS + "?user_id=\(withID)"
        ServiceManager.shared.request(type: PostAddressModel.self, url: postAddressUrl, method: .post, view: controller.view, parameters: parameters) { (result) in
            print("The post address result is :\(String(describing: result))")
            
            if let result = result, result.code == 200 {
                print("The response of post user address is :\(String(describing: result))")
//                self.UserAddressPostedList = result.response!
                completion(nil)
            }
            else {
                completion(result?.error ?? "")
            }
        }
        
    }
    
    
    func numberOfRowsForSection(_ at:Int)->Int{
        return self.addressList.count
    }
    
    func modelForRow(_ at:Int)->Address{
        return self.addressList[at]
    }
}

extension AddressViewModel {
    func removeAddressAt(_ index: Int){
        
    }
}

//
//  ProfileSecondCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ProfileSecondCell: UITableViewCell {

    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var secondLabel: UILabel!
    
    
    var secondData: ProfileSecondTableViewData? {
        didSet {
            self.secondImageView.image = UIImage(named: self.secondData?.imaegNameSecond ?? "" )
            self.secondLabel.text =  self.secondData?.titleSecond ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

//
//  ProfileFirstCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ProfileFirstCell: UITableViewCell {

    @IBOutlet weak var FirstimageView: UIImageView!
    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var firstDescriptionLable: UILabel!
    var FirstData : ProfileFirstTableViewData? {
        didSet {
            self.FirstimageView.image = UIImage(named: self.FirstData?.imageName ?? "")
            self.firstTitle.text = self.FirstData?.title ?? ""
            self.firstDescriptionLable.text = self.FirstData?.subTitle ?? ""
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

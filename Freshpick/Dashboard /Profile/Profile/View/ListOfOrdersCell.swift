//
//  ListOfOrdersCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 08/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ListOfOrdersCell: UITableViewCell {
    
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var orderStatusLable: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

//
//  SummeryView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SummeryView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        backgroundColor = .white
        self.setShadow()
        let scrollView = UIScrollView()
        addAutolayoutSubview(scrollView)
        
        let view: SummaryContentView = .fromNib()
        scrollView.addAutolayoutSubview(view)
        
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: self.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0),

            
            view.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            view.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 8),
            view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -16),            
        ])
    }
}

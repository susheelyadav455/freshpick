//
//  OrderItemsView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class OrderItemsView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        addAutolayoutSubview(scrollView)
        
        let relatedItems = RelatedItemsTableView()
        relatedItems.relatedItemsDelegate = self
        relatedItems.headerTitles = ["Related Items", "01 Items"]
        relatedItems.items = [.init(imageName: "mangoes",
                                    name: "Seasoned Mangoes",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "oil_masala",
                                    name: "Oil & Masala",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "chicken",
                                    name: "Chicken",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "pedegree",
                                    name: "Pedigree",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149")
        ]
        scrollView.addAutolayoutSubview(relatedItems)
        
        
        let relatedItems2 = RelatedItemsTableView()
        relatedItems2.relatedItemsDelegate = self
        relatedItems2.headerTitles = ["Related Items","02 Items"]
        relatedItems2.items = [.init(imageName: "mangoes",
                                    name: "Seasoned Mangoes",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "oil_masala",
                                    name: "Oil & Masala",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "chicken",
                                    name: "Chicken",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149"),
                              .init(imageName: "pedegree",
                                    name: "Pedigree",
                                    quantity: "500g",
                                    originalPrice: "Rs.180",
                                    discountedPrice: "149")
        ]
        scrollView.addAutolayoutSubview(relatedItems2)
        
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: self.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            relatedItems.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            relatedItems.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            relatedItems.topAnchor.constraint(equalTo: scrollView.topAnchor),
            relatedItems.bottomAnchor.constraint(equalTo: relatedItems2.topAnchor, constant: -16),
            
            relatedItems.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0),
        ])
        
        NSLayoutConstraint.activate([
            relatedItems2.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            relatedItems2.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            relatedItems2.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -16),
        ])
        
        print("Width Anchor is :\(self.frame)")
    }

}

extension OrderItemsView: RelatedItemCellDelegate {
    func selectquantity(sender: UIButton) {
        print("select Quantity: \(sender)")
    }
    
    
}

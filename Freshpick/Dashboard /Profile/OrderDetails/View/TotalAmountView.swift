//
//  TotalAmountView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class TotalAmountView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    let itemsCount: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let totalAmount: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private func setupView() {
        self.addAutolayoutSubview(itemsCount)
        self.addAutolayoutSubview(totalAmount)
        
        itemsCount.setContentHuggingPriority(.init(rawValue: 249), for: .horizontal)
        itemsCount.setContentCompressionResistancePriority(.init(rawValue: 748), for: .horizontal)
        
        apply(itemsCount) {
            $0.text = "Items: 05"
            $0.textColor = kHeadingTitleTextColor
            $0.font = UIFont(name: kMuliBold, size: 15)
            $0.textAlignment = .left
            $0.numberOfLines = 0
        }
        
        apply(totalAmount) {
            $0.text = "Total Amount: Rs.1,299"
            $0.textColor = kHeadingTitleTextColor
            $0.font = UIFont(name: kMuliBold, size: 15)
            $0.textAlignment = .right
            $0.numberOfLines = 0
        }
        
        NSLayoutConstraint.activate([
            itemsCount.topAnchor.constraint(equalTo: self.topAnchor, constant: 14),
            
            itemsCount.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -14),
            itemsCount.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 17),
        ])
        
        NSLayoutConstraint.activate([
            totalAmount.topAnchor.constraint(equalTo: itemsCount.topAnchor),
            totalAmount.leadingAnchor.constraint(equalTo: itemsCount.trailingAnchor, constant: 10),
            totalAmount.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -17),
            totalAmount.bottomAnchor.constraint(equalTo: itemsCount.bottomAnchor),
            
        ])
    }
}

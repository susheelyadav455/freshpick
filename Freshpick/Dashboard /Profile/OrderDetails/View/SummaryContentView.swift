//
//  SummaryContentView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SummaryContentView: UIView {

    @IBOutlet var successView: UIView!
    @IBOutlet var deliveryDateObj: UILabel!
    
    @IBOutlet var deiveryTimeObj: UILabel!
    @IBOutlet var personName: UILabel!
    @IBOutlet var personPhoneNumber: UILabel!
    @IBOutlet var personAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setupUI(){
        successView.layer.cornerRadius = 10
    }
}

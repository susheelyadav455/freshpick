//
//  OrderDetailsViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 15/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {
    private var selectedIndex = 0
    
    let themeCollectionView: FPThemeCollectionView = {
        let collectionView = FPThemeCollectionView(collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.items = ["Summary","Items"]
        return collectionView
    }()
    
    let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    let totalAmountView: TotalAmountView = {
        let view = TotalAmountView()
        view.setShadow()
        return view
    }()
    
    let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 10)
    
    let orderItemsView = OrderItemsView()
    let summayView = SummeryView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        navigationItem.title = "Order Details"
        addSubViews()
        setupUI()
        addAutoLayouts()
        setupHiddenView()
    }
}

extension OrderDetailsViewController{
    func addSubViews() {
        containerView.addAutolayoutSubview(orderItemsView)
        containerView.addAutolayoutSubview(summayView)
        view.addAutolayoutSubview(stackView)
        stackView.addArrangedAutolayoutSubviews([themeCollectionView, containerView, totalAmountView])
    }
    
    func setupHiddenView() {
        if selectedIndex == 0{
            orderItemsView.isHidden = true
            summayView.isHidden = false
            totalAmountView.isHidden = true
        } else {
            orderItemsView.isHidden = false
            summayView.isHidden = true
            totalAmountView.isHidden = false
        }
    }
    
    func setupUI() {
        apply(themeCollectionView) {
            $0.fpThemeDelegate = self
            $0.selectItem(at: IndexPath.init(row: selectedIndex, section: 0), animated: true, scrollPosition: .left)
        }
    }
    
    func addAutoLayouts(){
        NSLayoutConstraint.activate([
            themeCollectionView.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10),
            stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16),
            stackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
        ])
        
        NSLayoutConstraint.activate([
            orderItemsView.topAnchor.constraint(equalTo: containerView.topAnchor),
            orderItemsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            orderItemsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            orderItemsView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            summayView.topAnchor.constraint(equalTo: containerView.topAnchor),
            summayView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            summayView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            summayView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])
    }
}

extension OrderDetailsViewController: CellSelectionDelegate {
    func cellSelected(indexPath: IndexPath, item: AnyObject) {
        selectedIndex = indexPath.row
        setupHiddenView()
        print("\(selectedIndex)")
    }
}

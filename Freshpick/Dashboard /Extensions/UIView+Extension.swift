//
//  UIView+Extension.swift
//  Freshpick
//
//  Created by Ajeet N on 14/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

extension UIView {
    func roundCorner(corners: UIRectCorner, radius: CGFloat, maskToBounds: Bool = false, shadowOffset: CGSize = CGSize(width: 0, height: 1), shadowColor: UIColor = UIColor(red: 170, green: 170, blue: 170), shadowOpacity: Float = 0.4) {
        let mask = CAShapeLayer()
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        mask.fillColor = UIColor.white.cgColor
        mask.path = path.cgPath
        mask.shadowRadius = radius
        mask.shadowOffset = shadowOffset
        mask.shadowColor = shadowColor.cgColor
        mask.shadowOpacity = shadowOpacity
        mask.shadowPath = mask.path
        layer.insertSublayer(mask, at: 0)
    }
    
    func setShadow(cornerRadius: CGFloat = 10, maskToBounds: Bool = false, shadowRadius: CGFloat = 10, shadowColor: UIColor = UIColor(red: 170, green: 170, blue: 170), shadowOpacity: Float = 0.4, shadowOffset: CGSize = CGSize(width: 0, height: 1)){
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = maskToBounds
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.backgroundColor = .white
    }
}

extension UIView {
    func addAutolayoutSubview(_ view: UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
    
    func apply<T:UIView>(_ view: T, completion:((T))->Void){
        completion(view)
    }
}

extension UIView {
    @discardableResult
    func addEmptyState(type: EmptyStateView.ViewType)-> UIView {
        let emptyStateView: EmptyStateView = UIView.fromNib()
        emptyStateView.viewType = type//.address
        self.addAutolayoutSubview(emptyStateView)
        
        NSLayoutConstraint.activate([
            emptyStateView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            emptyStateView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            emptyStateView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
            emptyStateView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        return emptyStateView
    }
}

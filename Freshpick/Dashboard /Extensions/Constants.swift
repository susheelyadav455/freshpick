//
//  Constants.swift
//  Freshpick
//
//  Created by Ajeet N on 15/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

let kDarkTextColor = UIColor(red: 39, green: 45, blue: 47)
let kRedColor = UIColor(red: 255, green: 64, blue: 59)
let kDescriptionTextColor = UIColor(red: 101, green: 101, blue: 101)
let kHeadingTitleTextColor = UIColor(red: 181, green: 181, blue: 181)
let kPageControllerColor = UIColor(red: 215, green: 215, blue: 215)



let kMuliBold = "Muli-Bold"
let kMuliMedium = "Muli-Medium"
let kMuliExtraBold = "Muli-ExtraBold"

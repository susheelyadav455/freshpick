//
//  UIStackView+Extension.swift
//  Freshpick
//
//  Created by Ajeet N on 14/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedAutolayoutSubview(_ view: UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        addArrangedSubview(view)
    }
    
    func addArrangedAutolayoutSubviews(_ view: [UIView]){
        view.forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addArrangedSubview($0)
        }
    }
}

extension UIStackView {
    convenience init(alignment: UIStackView.Alignment, distribution: UIStackView.Distribution, axis: NSLayoutConstraint.Axis, spacing: CGFloat) {
        self.init()
        self.axis = axis
        self.alignment = alignment
        self.distribution = distribution
        self.spacing = spacing
    }
}

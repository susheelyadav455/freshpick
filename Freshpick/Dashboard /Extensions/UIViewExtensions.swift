//
//  UIViewExtensions.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import UIKit


class CustomView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var maskToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor {
        get {
            return UIColor.init(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get{
            return layer.shadowOpacity
        }
        set{
            layer.shadowOpacity = newValue
        }
    }
}

extension CustomView {
    func setDefaultShadow(cornerRadius: CGFloat = 10, shadowRadius: CGFloat = 10, shadowColor: UIColor = UIColor(red: 170, green: 170, blue: 170), shadowOpacity: Float = 0.4, shadowOffset: CGSize = CGSize(width: 0, height: 1)){
        self.cornerRadius = cornerRadius
        self.maskToBounds = false
        self.shadowRadius = shadowRadius
        self.shadowOffset = shadowOffset
        self.shadowColor = shadowColor
        self.shadowOpacity = shadowOpacity
    }
}

extension UIViewController {
    func apply<T:UIView>(_ view: T, completion:((T))->Void){
        completion(view)
    }
}

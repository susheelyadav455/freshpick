//
//  UITextFieldExtensions.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
public class CustomTextField : UITextField {
      let cornerLayer = CAShapeLayer()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.mask = cornerLayer
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    

    @IBInspectable var leftImage : UIImage? {
         didSet {
             if let image = leftImage{
                 leftViewMode = .always
                 let imageView = UIImageView(frame: CGRect(x: 16, y: 8, width: 20, height: 20))
                 imageView.image = image
                 imageView.tintColor = tintColor
                 let view = UIView(frame : CGRect(x: bounds.size.width - 80, y: 0, width: 45, height: 40))
                 view.addSubview(imageView)
                 leftView = view
             }else {
                 leftViewMode = .never
             }

         }
     }
    
    @IBInspectable var rightImage : UIImage? {
         didSet {
             if let image = rightImage{
                 rightViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 0, y: 5, width: 30, height: 30))
                 imageView.image = image
                 imageView.tintColor = tintColor
                 let view = UIView(frame : CGRect(x: bounds.size.width - 80, y: 0, width: 45, height: 40))
                 view.addSubview(imageView)
                 rightView = view
             }else {
                 rightViewMode = .never
             }

         }
     }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0

    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height)
    }

    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

extension UIViewController
{
    func validateTextField(textField: CustomTextField){
        if textField.text?.count ?? 0 > 0{
            textField.borderColor = .gradientColor1
        }else{
            textField.borderColor = .gradientColor2
        }
    }
}



@IBDesignable
public class CustomTField : UITextField {
    public let shapeLayer = CAShapeLayer()
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.borderStyle = .none
        setTopRightAndBottomLeftCorners()
        self.shapeLayer.frame = self.bounds
    }
    
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var leftImage : UIImage? {
         didSet {
             if let image = leftImage{
                 leftViewMode = .always
                 let imageView = UIImageView(frame: CGRect(x: 16, y: 8, width: 20, height: 20))
                 imageView.image = image
                 imageView.tintColor = tintColor
                 let view = UIView(frame : CGRect(x: bounds.size.width - 80, y: 0, width: 45, height: 40))
                 view.addSubview(imageView)
                 leftView = view
             }else {
                 leftViewMode = .never
             }

         }
     }
    
    @IBInspectable var rightImage : UIImage? {
         didSet {
             if let image = rightImage{
                 rightViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 0, y: 5, width: 30, height: 30))
                 imageView.image = image
                 imageView.tintColor = tintColor
                 let view = UIView(frame : CGRect(x: bounds.size.width - 80, y: 0, width: 45, height: 40))
                 view.addSubview(imageView)
                 rightView = view
             }else {
                 rightViewMode = .never
             }

         }
     }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0

    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height)
    }

    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}

extension CustomTField  {
    
    func setTopRightAndBottomLeftCorners(){
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
         shapeLayer.path = path.cgPath
         shapeLayer.frame = bounds
        shapeLayer.strokeColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
        shapeLayer.cornerRadius = 1.0
         shapeLayer.fillColor = UIColor.white.cgColor
        self.layer.mask = shapeLayer
        self.layer.insertSublayer(shapeLayer, at: 0)
    }
}


@IBDesignable
public class CustomTextView : UITextView {
    public let shapeLayer = CAShapeLayer()
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderColor = UIColor.clear.cgColor
        setTopRightAndBottomLeftCorners()
        self.shapeLayer.frame = self.bounds
    }
}

extension CustomTextView{
    func setTopRightAndBottomLeftCorners(){
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomLeft], cornerRadii: CGSize(width: 15, height: 15))
         shapeLayer.path = path.cgPath
         shapeLayer.frame = bounds
        shapeLayer.strokeColor = UIColor.init(red: 118/255.0, green: 189/255.0, blue: 67/255.0, alpha: 1.5).cgColor
        shapeLayer.cornerRadius = 1.0
         shapeLayer.fillColor = UIColor.white.cgColor
        self.layer.mask = shapeLayer
        self.layer.insertSublayer(shapeLayer, at: 0)
    }
}

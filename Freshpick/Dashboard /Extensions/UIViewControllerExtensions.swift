//
//  UIViewControllerExtensions.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift
import CoreLocation


extension UIViewController {
    
    func alert(title: String,message: String,actionTitles: [String],actionStyle: [UIAlertAction.Style],action: [((UIAlertAction) -> Void)]) {
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: actionStyle[index], handler: action[index])
            alert.addAction(action) }
        self.present(alert, animated: true, completion: nil)
    }
    
    func alert(title: String,message: String) {
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}


//MARK: Toast Message
extension NSObject {
    func toast(msgString:String, view:UIView) {
        DispatchQueue.main.async {
            view.makeToast(msgString)
        }
    }
    func toastWithDurationAndAlignment(msg:String, duration: TimeInterval, position: ToastPosition, view:UIView){
        DispatchQueue.main.async {
            view.makeToast(msg, duration: duration, position: position)
        }
    }
}

//MARK: UIActivity Indicator View Method
extension NSObject {
    func showActivityIndicator(view: UIView) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.color = .black
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
        return activityIndicator
    }
    
    func removeActivityindicator(indicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            indicator.stopAnimating()
        }
    }
    
    func removeActivityindicatorWithDelay(indicator: UIActivityIndicatorView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            indicator.stopAnimating()
        })
    }
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}


extension UIViewController
{
    func logout(){
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            guard let loginController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
//                        fatalError("View Controller not found")
//                    }
//
//            let navigationController = UINavigationController(rootViewController: loginController)
//            navigationController.modalPresentationStyle = .fullScreen
//            self.present(navigationController, animated: false, completion: nil)
//        }
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension UIViewController
{
    func getAddressFromLatLons(pdblLatitude: String, withLongitude pdblLongitude: String, completion completionHandler:@escaping(String) -> Void) {

        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    DispatchQueue.main.async {
                        completionHandler(addressString)
                    }
              }
        })
    }
}
extension UIViewController
{
    //Get Path
    func getPath() -> String {
      let plistFileName = "UserDetails.plist"
      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
      let documentPath = paths[0] as NSString
      let plistPath = documentPath.appendingPathComponent(plistFileName)
      return plistPath
    }
}

//extension UICollectionViewCell {
//    func getuserDetails() -> UserDetails{
//        let request : NSFetchRequest<UserDetails> = UserDetails.fetchRequest()
//            let saveDetials = try! context.fetch(request)
//        return saveDetials[0]
//    }
//}

extension UIViewController
{
//    func showAlert(message: String){
//        guard let alertController = storyboard?.instantiateViewController(withIdentifier: "customAlertController") as? CustomAlertController else {
//                fatalError("View Controller not found")
//        }
//        alertController.message = message
//        alertController.modalPresentationStyle = .overCurrentContext
//        alertController.modalPresentationStyle = .overFullScreen
//        self.present(alertController, animated: false, completion: nil)
//    }
    
//    func showAlert(message: String,storyBoard:UIStoryboard){
//        guard let alertController = storyBoard.instantiateViewController(withIdentifier: "customAlertController") as? CustomAlertController else {
//                fatalError("View Controller not found")
//        }
//        alertController.message = message
//        alertController.modalPresentationStyle = .overCurrentContext
//        alertController.modalPresentationStyle = .overFullScreen
//        self.present(alertController, animated: false, completion: nil)
//    }
}
extension UIViewController
{
//    func session(){
//        guard let sessionExpiredController = storyboard?.instantiateViewController(withIdentifier: "sessionExpiredController") as? SessionExpiredController else {
//                fatalError("View Controller not found")
//        }
//        setTimer?.invalidate()
//        setTimer = nil
//        sessionExpiredController.modalPresentationStyle = .overCurrentContext
//        sessionExpiredController.modalPresentationStyle = .overFullScreen
//        self.present(sessionExpiredController, animated: false, completion: nil)
//    }
    
    func navigationBackTitle(title:String){
        let backButton = UIBarButtonItem()
        backButton.title = title
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
}


//extension String {
//    
//func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
//
//    return ceil(boundingBox.height)
//  }
//}

extension LosslessStringConvertible {
    var string: String { .init(self) }
}


extension String {
   
    func chopPrefix(_ count: Int = 1) -> String {
           return count>self.count ? self : String(self[index(self.startIndex, offsetBy: count)...])
       }
    
    func toLengthOf(length:Int) -> String {
        if length <= 0 {
            return self
        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
            return self.substring(from: to)

        } else {
            return ""
        }
    }
    
}
extension NSObject {
    public func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    public func trimWhiteSpace(txt:String)->String{
        return txt.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    public func roundCorner(_ corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        
        self.layoutIfNeeded()
        
        let path = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))

        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask

        let borderPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let borderLayer = CAShapeLayer()
        borderLayer.path = borderPath.cgPath
        borderLayer.lineWidth = borderWidth
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = self.bounds
        self.layer.addSublayer(borderLayer)
    }
}

extension CharacterSet {

    public static let urlQueryParameterAllowed = CharacterSet.urlQueryAllowed.subtracting(CharacterSet(charactersIn: "&?~!$*(.,)_-+':"))

    public static let urlQueryDenied           = CharacterSet.urlQueryAllowed.inverted()
    public static let urlQueryKeyValueDenied   = CharacterSet.urlQueryParameterAllowed.inverted()
    public static let urlPathDenied            = CharacterSet.urlPathAllowed.inverted()
    public static let urlFragmentDenied        = CharacterSet.urlFragmentAllowed.inverted()
    public static let urlHostDenied            = CharacterSet.urlHostAllowed.inverted()

    public static let urlDenied                = CharacterSet.urlQueryDenied
        .union(.urlQueryKeyValueDenied)
        .union(.urlPathDenied)
        .union(.urlFragmentDenied)
        .union(.urlHostDenied)
    public func inverted() -> CharacterSet {
        var copy = self
        copy.invert()
        return copy
    }
}

public extension String {
    func urlEncoded(denying deniedCharacters: CharacterSet = .urlDenied) -> String? {
        return addingPercentEncoding(withAllowedCharacters: deniedCharacters.inverted())
    }
}


extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}

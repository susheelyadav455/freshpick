//
//  OffersVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class OffersVC: UIViewController {
    private let offerCollectionView = FPThemeCollectionView(collectionViewLayout: UICollectionViewFlowLayout())
    private let searchBar:FPSearchBar = .fromNib()
    private var selectedIndex = 0

    @IBOutlet var scrollViewTopConstrain: NSLayoutConstraint!
    @IBOutlet var scrollViewObj: UIScrollView!
    @IBOutlet var scrollContentView: UIView!
    
    private var allCategoryView: CategoryView!
//    private let allOfferView = RelatedItemsTableView()

    let viewModel = GroupViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.allCategoryView = CategoryView(delegate: self, productCategory: CategoryType.offers)
        navigationItem.title = "Offers"
        view.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        scrollContentView.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        addOfferHorizontalScrollView()
        addSearchBar()
        addOfferByCategoryView()
        addOfferViewsToScrollView()
        
        viewModel.getGroups(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if error == nil {
                let items = self.viewModel.getGroupsAsString()
                self.offerCollectionView.items = items
                self.offerCollectionView.selectItem(at: IndexPath.init(row: self.selectedIndex , section: 0), animated: true, scrollPosition: .left)
                self.updateCategory()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-ExtraBold", size: 32) ??
                    UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func updateCategory() {
        if let subGroups = self.viewModel.subGroupForGroupAt(self.selectedIndex) {
            self.allCategoryView.items = subGroups.map({
                CategoryItems(name: $0.name, details: nil, image: nil, urlImage: $0.image)
            })
            self.allCategoryView.isHidden = false
        }
        self.view.layoutIfNeeded()
    }
    
    private func addOfferHorizontalScrollView(){
        self.view.addAutolayoutSubview(offerCollectionView)
        offerCollectionView.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        offerCollectionView.fpThemeDelegate = self
        
        NSLayoutConstraint.activate([
            offerCollectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor,constant: 0),
            offerCollectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,constant: 10),
            offerCollectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,constant: -10),
            offerCollectionView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    private func addSearchBar(){
        self.view.addAutolayoutSubview(searchBar)
        searchBar.fpSearchBar = self
        searchBar.navigateOnBecomeFirstResponder = true
        searchBar.setShadow()
        
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: offerCollectionView.bottomAnchor,constant: 20),
            searchBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,constant: 18),
            searchBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,constant: -18),
            searchBar.heightAnchor.constraint(equalToConstant: 40)
        ])
        self.view.removeConstraint(self.scrollViewTopConstrain)
        NSLayoutConstraint.activate([
            self.scrollViewObj.topAnchor.constraint(equalTo: searchBar.bottomAnchor,constant: 10)
        ])
    }
    
    private func addOfferByCategoryView(){
        allCategoryView.numberOfColumns = 3
        allCategoryView.cellHeight = 173
        allCategoryView.isHeaderVisible = true
        allCategoryView.isOfferViewHidden = true
        allCategoryView.headerTitle = "Offers by categories"
        allCategoryView.headerButtonTitle = nil
        allCategoryView.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
    }
    
    private func addOfferViewsToScrollView(){
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 0)
        scrollContentView.addAutolayoutSubview(stackView)
        [allCategoryView].compactMap({ $0 }).forEach {
            stackView.addArrangedAutolayoutSubview($0)
            $0.isHidden = true
        }
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollContentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor,constant: 16),
            stackView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor,constant: -16),
            stackView.bottomAnchor.constraint(equalTo: scrollContentView.bottomAnchor)
        ])
    }
}

extension OffersVC: CellSelectionDelegate {
    func cellSelected(indexPath: IndexPath, item: AnyObject) {
        print("Selected Category Index :\(indexPath.row)")
        selectedIndex = indexPath.row
        updateCategory()
    }
}

extension OffersVC: FPSearchBarDelegate{
    func searchBarShouldBeginEditing(_ textField: UITextField, _ shouldEdit: Bool) {
        if shouldEdit {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func searchButtonClicked(_ text: String) {
        print("Search Button clicked :\(text)")
    }
    
    func searchBarText(_ text: String) {
        print("Search text :\(text)")
    }
}

extension OffersVC: RelatedItemCellDelegate{
    func selectquantity(sender: UIButton) {
        print("500g tapped :\(sender.tag)")
    }
}

extension OffersVC: CategoryCollectionViewSelection {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType) {
        print("OffersVC selected index :\(index) item :\(item) productCategory :\(productCategory)")
        
        let subGroups = self.viewModel.subGroupForGroupAt(selectedIndex)
        let subGroup = subGroups?[index]
        let productListVC = self.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
        productListVC.items = subGroup
        productListVC.navigationBarTitle = subGroup?.name
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
}

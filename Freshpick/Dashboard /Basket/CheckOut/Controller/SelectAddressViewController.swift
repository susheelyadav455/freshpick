//
//  SelectAddressViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 17/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol SelectAddressDelegate: AnyObject {
    func selectedAddress(_ address: String)
}

class SelectAddressViewController: UIViewController {
    weak var selectAddressDelegate: SelectAddressDelegate?
    private let selectAddressTableView = UITableView()
    let viewModel = AddressViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let customNavigationView = CustomNavigationView()
        customNavigationView.backgroundColor = UIColor(red: 241, green: 241, blue: 241, a: 1)
        customNavigationView.navigationBarHeight = 60
        customNavigationView.leadingButtonTitle = "Cancel"
        customNavigationView.navigationBarTitle = "Select Address"
        
        let horizontalStackView = UIStackView(alignment: .fill, distribution: .fill, axis: .horizontal, spacing: 0)

        let selectButton = UIButton()
        selectButton.backgroundColor = UIColor(red: 255, green: 199, blue: 0)
        selectButton.setTitleColor(.black, for: .normal)
        selectButton.setTitle("PROCEED", for: .normal)
        selectButton.titleLabel?.font = UIFont(name: kMuliExtraBold, size: 14)
        selectButton.layer.cornerRadius = 10
        selectButton.addTarget(self, action: #selector(self.proceedButtonAction(sender:)), for: .touchUpInside)
        
        [helperWidthView(84),selectButton,helperWidthView(84)].forEach {
            horizontalStackView.addArrangedAutolayoutSubview($0)
        }
        
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 10)
        view.addAutolayoutSubview(stackView)
        
        [customNavigationView,selectAddressTableView,horizontalStackView].forEach {
            stackView.addArrangedAutolayoutSubview($0)
        }
        
        selectAddressTableView.dataSource = self
        selectAddressTableView.delegate = self
        selectAddressTableView.separatorStyle = .none
        selectAddressTableView.backgroundColor = UIColor(red: 252, green: 252, blue: 252)
        selectAddressTableView.register(UINib.init(nibName: "SelectCheckoutAddressCell", bundle: nil), forCellReuseIdentifier: "SelectCheckoutAddressCell")
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -40),
            
            selectButton.heightAnchor.constraint(equalToConstant: 50),
        ])
        
        viewModel.getAddressList(withId: "2", controller: self) {[weak self] (result) in
            debugPrint("The address result is :\(String(describing: result))")
            guard let self = self else { return }
            if result == nil {
                self.selectAddressTableView.reloadData()
            }
            else {
                self.toast(msgString: result ?? "", view: self.view)
            }
        }
    }
    
    @objc private func proceedButtonAction(sender: UIButton) {
        if let indexPath = self.selectAddressTableView.indexPathForSelectedRow, let cell = self.selectAddressTableView.cellForRow(at: indexPath) as? SelectCheckoutAddressCell{
//            cell.address.text = addressString
            self.selectAddressDelegate?.selectedAddress(cell.address.text ?? "")
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension SelectAddressViewController: UITableViewDelegate{
    
}

extension SelectAddressViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCheckoutAddressCell") as! SelectCheckoutAddressCell
        
        let address = viewModel.modelForRow(indexPath.row)
        var addressString = ""
        
        if let houseNo = address.houseNo {
            addressString.append(houseNo)
        }
        
        if let locality = address.locality {
            addressString.append(", \(locality)")
        }
        
        if let landmark = address.landmark {
            addressString.append(", \(landmark)")
        }
        
        if let city = address.city {
            addressString.append(", \(city)")
        }
        
        if let pincode = address.pincode {
            addressString.append(", \(pincode)")
        }
        
        cell.address.text = addressString
        cell.addressType.text = address.addressType
        
        return cell
    }
}

extension UIViewController{
    func helperWidthView(_ width: CGFloat)->UIView{
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.widthAnchor.constraint(equalToConstant: width).isActive = true
        return vw
    }
    
    func helperHeightView(_ height: CGFloat)->UIView{
        let vw = UIView()
        vw.backgroundColor = .yellow
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.heightAnchor.constraint(equalToConstant: height).isActive = true
        return vw
    }
}

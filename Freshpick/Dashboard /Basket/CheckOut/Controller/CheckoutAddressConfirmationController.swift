//
//  CheckoutAddressConfirmationController.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CheckoutAddressConfirmationController: UIViewController {
    let checkOutContainer = CheckoutAddressConfirmationContainerView()
    let addressContainer = CheckoutAddressContainerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Checkout"
        
        addressContainer.changeDelegate = self
        
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 0)
        view.addAutolayoutSubview(stackView)
        
        stackView.addArrangedAutolayoutSubview(checkOutContainer)
        stackView.addArrangedAutolayoutSubview(addressContainer)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }

}

extension CheckoutAddressConfirmationController: ChangeAddressDelegate{
    func changeAddressTapped(sender: UIButton) {
        let vc = SelectAddressViewController()
        vc.selectAddressDelegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension CheckoutAddressConfirmationController: SelectAddressDelegate{
    func selectedAddress(_ address: String) {
        addressContainer.addressSelectionView.addressField.text = address
    }
    
    
}

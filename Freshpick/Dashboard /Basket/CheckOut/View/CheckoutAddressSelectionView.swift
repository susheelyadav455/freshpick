//
//  CheckoutAddressSelectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

protocol ChangeAddressDelegate:class {
    func changeAddressTapped(sender: UIButton)
}

class CheckoutAddressSelectionView: UIView {
    @IBOutlet var changeObj: UIButton!
    @IBOutlet var addressField: UILabel!
    weak var changeDelegate: ChangeAddressDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI() {
        
    }

    @IBAction func changeObjectActinon(_ sender: UIButton) {
        changeDelegate?.changeAddressTapped(sender: sender)
    }
}

//
//  CheckoutAddressContainerView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CheckoutAddressContainerView: UIView {
    let totalAmountView:CheckoutTotalAmountView = .fromNib()
    let addressSelectionView:CheckoutAddressSelectionView = .fromNib()
    weak var changeDelegate: ChangeAddressDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI() {
        addressSelectionView.changeDelegate = self
        self.setShadow()
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 0)
        self.addAutolayoutSubview(stackView)
        
        stackView.addArrangedAutolayoutSubview(addressSelectionView)
        stackView.addArrangedAutolayoutSubview(totalAmountView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 17),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -17),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }

}

extension CheckoutAddressContainerView: ChangeAddressDelegate{
    func changeAddressTapped(sender: UIButton) {
        changeDelegate?.changeAddressTapped(sender: sender)
    }
    
    
}

//
//  CheckoutDeliveryOptionView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CheckoutDeliveryOptionView: UIView {

    @IBOutlet var applyObj: UIButton!
    @IBOutlet var tomorrowShadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI() {
        self.setShadow()
        tomorrowShadowView.layer.cornerRadius = 8
        tomorrowShadowView.layer.borderWidth = 1
        tomorrowShadowView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, a: 0.1).cgColor
        
        applyObj.layer.cornerRadius = 8
    }

    @IBAction func applyAction(_ sender: UIButton) {
        
    }
    
}

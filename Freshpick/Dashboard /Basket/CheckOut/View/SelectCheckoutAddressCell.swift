//
//  SelectCheckoutAddressCell.swift
//  Freshpick
//
//  Created by Ajeet N on 20/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class SelectCheckoutAddressCell: UITableViewCell {

    @IBOutlet var radioButton: UIButton!
    @IBOutlet var addressType: UILabel!
    @IBOutlet var address: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            //radioButton.backgroundColor = UIColor(red: 255, green: 64, blue: 59)
            radioButton.isSelected = true
        } else {
            //radioButton.backgroundColor = UIColor.lightGray
            radioButton.isSelected = false
        }
    }
    
}

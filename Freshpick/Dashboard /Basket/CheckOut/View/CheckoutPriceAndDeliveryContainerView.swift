//
//  CheckoutPriceAndDeliveryContainerView.swift
//  Freshpick
//
//  Created by Ajeet N on 16/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CheckoutPriceAndDeliveryContainerView: UIView {
    let deliveryOptionView: CheckoutDeliveryOptionView = .fromNib()
    let basketVaueView: CheckoutBasketValueView = .fromNib()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI() {
        let stackView = UIStackView(alignment: .fill, distribution: .fillProportionally, axis: .vertical, spacing: 20)
        self.addAutolayoutSubview(stackView)
        
        stackView.addArrangedAutolayoutSubview(deliveryOptionView)
        stackView.addArrangedAutolayoutSubview(basketVaueView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 17),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -17),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }

}

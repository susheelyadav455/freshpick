//
//  BasketHeaderCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 25/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class BasketHeaderCell: UITableViewCell {

    @IBOutlet weak var basketHeaderName: UILabel!
    @IBOutlet weak var basketItemsCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

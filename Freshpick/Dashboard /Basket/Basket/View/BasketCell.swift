//
//  BasketCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 25/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class BasketCell: UITableViewCell {

    
    @IBOutlet weak var basketBackgroundView: CustomView!
    @IBOutlet weak var basketProductImage: UIImageView!
    @IBOutlet weak var basketBrandName: UILabel!
    @IBOutlet weak var basketFinalPrice: UILabel!
    @IBOutlet weak var basketProductName: UILabel!
    @IBOutlet weak var baksetQuantity: UIButton!
    @IBOutlet weak var basketAddOutlet: UIButton!
    @IBOutlet var payablePrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  BasketVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 22/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class BasketVC: UIViewController {
    
    //private var images = ["pac", "slv", "pac", "slv", "pac", "slv", "pac", "slv"]
    //private var HeaderSectionNames = ["Fruits", "Fruits and Vegetables", "Pulses"]
    let viewModel = CartViewModel()
    
    @IBOutlet weak var basketTableViewObj: UITableView! {
        didSet{
            self.basketTableViewObj.delegate = self
            self.basketTableViewObj.dataSource = self
        }
    }
    
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Basket"
    }
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-ExtraBold", size: 32) ??
                    UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
        self.viewModel.getCartItemsList(withId: ConstantValues.shared.userId, controller: self) { (error) in
            if error == nil {
                self.basketTableViewObj.reloadData()
            }
        }
    }
    
    
    @IBAction func checkOutAction(_ sender: CustomButton) {
        let controller = CheckoutAddressConfirmationController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

//MARK: UITableView Delegate and UITableView Datasource Methods
extension BasketVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BasketCell", for: indexPath) as? BasketCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        let cart = self.viewModel.modelForRow(indexPath.row)
        
        if let image = cart.image {
            let url = URL(string: image)
            cell.basketProductImage.kf.setImage(with: url)
        } else {
            cell.basketProductImage.image = UIImage(named: "")
        }
        cell.basketBrandName.text = cart.brand
        cell.basketProductName.text = cart.name
        cell.baksetQuantity.setTitle(cart.weightOrQuantity, for: .normal)
        cell.payablePrice.text = "Rs.\(cart.amount ?? 0)"
        cell.basketFinalPrice.attributedText = "Rs.\(cart.actualAmount ?? 0)".strikeThrough()
        
//        {
//            "productCode": 10,
//            "name": "mattel inc.",
//            "image": "",
//            "brandCode": "6",
//            "quantity": 5,
//            "productQuantityCode": 8,
//            "weightOrQuantity": "500g",
//            "amount": 25,
//            "actualAmount": 30,
//            "brand": "telephone & data systems inc",
//            "subGroup": "the allstate corporation"
//        }
        
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerCell =  Bundle.main.loadNibNamed("BasketHeaderCell", owner: self, options: nil)?.first as! BasketHeaderCell
//        headerCell.basketHeaderName.text = HeaderSectionNames[section]
//        return headerCell
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 148
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // 1. Set the initial state of the cell
        cell.alpha = 0
        
        // 2. UIView animation Method to change to final state of the cell
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
        }
    }
    
}

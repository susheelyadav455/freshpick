//
//  CartViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 09/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CartViewModel {
    //private var handler: ((_ error: String?)->Void)?
    private var cartList = [Cart]()
    
    func getCartItemsList(withId:String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        //self.handler = completion//userId=2
        var url = URL(string: API.BASEURL+API.GETCART)!//+"?user_id=\(withId)"   //  ?user_id=2
        url.appendQueryItem(name: "userId", value: withId)
        
        ServiceManager.shared.request(type: CartModel.self, url: url.absoluteString, method: .get, controller: controller) { (result) in
            if let result = result, result.response?.items != nil {
                print("Address List result :\(result)")
                self.cartList = result.response!.items!
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func numberOfRowsForSection(_ at:Int)->Int{
        return self.cartList.count
    }
    
    func modelForRow(_ at:Int)->Cart{
        return self.cartList[at]
    }
}

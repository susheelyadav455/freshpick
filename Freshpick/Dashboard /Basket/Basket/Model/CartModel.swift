//
//  CartModel.swift
//  Freshpick
//
//  Created by Ajeet N on 09/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//



import Foundation

// MARK: - CartModel
struct CartModel: Codable {
    let code: Int?
    let error: String?
    let response: CartResponse?
}

// MARK: - Response
struct CartResponse: Codable {
    let items: [Cart]?
    let basketValue: Int?
}

// MARK: - Item
struct Cart: Codable {
    let productCode: Int?
    let name, image, brandCode: String?
    let quantity, productQuantityCode: Int?
    let weightOrQuantity: String?
    let amount, actualAmount: Int?
    let brand, subGroup: String?
}

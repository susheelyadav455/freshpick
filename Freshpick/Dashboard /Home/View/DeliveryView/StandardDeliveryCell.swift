//
//  StandardDeliveryCell.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class StandardDeliveryCell: UICollectionViewCell {
    @IBOutlet var deliveryImageObj: UIImageView!
    @IBOutlet var deliveryNameObj: UILabel!
    @IBOutlet var deliveryDateAndTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 12
    }

}

//
//  PastOrderCell.swift
//  Freshpick
//
//  Created by Ajeet N on 20/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class PastOrderCell: UITableViewCell {
    @IBOutlet var itemImage: UIImageView!
    @IBOutlet var itemName: UILabel!
    @IBOutlet var itemQuantity: UIButton!
    @IBOutlet var itemOriginalPrice: UILabel!
    @IBOutlet var itemDiscountedPrice: UILabel!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var brandName: UILabel!
    @IBOutlet var ratingObj: UILabel!
    @IBOutlet var assured: UIImageView!
    
    weak var relatedDelegate: RelatedItemCellDelegate?
    
    let stepper: GroceryStepper = {
        let vw = GroceryStepper()
        return vw
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addAutolayoutSubview(stepper)
        shadowView.setShadow(cornerRadius: 20, shadowRadius: 5)
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        NSLayoutConstraint.activate([
            stepper.trailingAnchor.constraint(equalTo: itemOriginalPrice.trailingAnchor),
            stepper.heightAnchor.constraint(equalToConstant: 40),
            stepper.widthAnchor.constraint(equalToConstant: 150),
            stepper.centerYAnchor.constraint(equalTo: itemQuantity.centerYAnchor)
        ])
        itemQuantity.addTarget(self, action: #selector(self.quantityAction(sender:)), for: .touchUpInside)
    }
    
    @objc func quantityAction(sender: UIButton){
        self.relatedDelegate?.selectquantity(sender: sender)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

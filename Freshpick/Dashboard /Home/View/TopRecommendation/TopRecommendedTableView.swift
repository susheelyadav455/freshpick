//
//  TopRecommendedTableView.swift
//  Freshpick
//
//  Created by Ajeet N on 24/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

protocol SwipeToRemoveDelegate: AnyObject {
    func swipeToRemove(indexPath: IndexPath, item: InItem)
}

class TopRecommendedTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    var headerButtonTitle: String?
    var item = [InItem]() {
        didSet{
            self.reloadData()
            print("===== TopRecommendedTableView content size :\(self.contentSize.height)")
        }
    }

    weak var selectionDelegate: PastOrderCellSelected?
    weak var relatedItemsDelegate: RelatedItemCellDelegate?
    weak var swipeDelegate: SwipeToRemoveDelegate?
    
    var headerTitles = [String]() {
        didSet {
            self.reloadSections(IndexSet.init(arrayLiteral: 0), with: .automatic)
        }
    }

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: .plain)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        self.delegate = self
        self.dataSource = self
        self.isScrollEnabled = false
        self.register(UINib.init(nibName: "PastOrderCell", bundle: nil), forCellReuseIdentifier: "PastOrderCell")   //   PastOrderCell
        self.backgroundColor = .clear
        self.separatorStyle = .none
        self.estimatedRowHeight = 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PastOrderCell", for: indexPath) as! PastOrderCell
        let localItem = item[indexPath.row]
        if let image = localItem.image?.percentileEncode() {
            let url = URL(string: image)
            cell.itemImage.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"))
        }
        cell.selectionStyle = .none
        cell.itemName.text = localItem.name
        cell.itemOriginalPrice.attributedText = "Rs. \(localItem.actualAmount ?? 0)".strikeThrough()
        cell.itemDiscountedPrice.text = "Rs. \(localItem.amount ?? 0)"
        cell.itemQuantity.setTitle(localItem.weightOrQuantity, for: .normal)
        cell.relatedDelegate = self
        cell.itemQuantity.tag = indexPath.row
        
        cell.brandName.text = localItem.brand
        if let assured = localItem.isAssured {
            cell.assured.isHidden = !assured
        } else {
            cell.assured.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 10)
        stackView.isLayoutMarginsRelativeArrangement = true
        
        for (index,text) in headerTitles.enumerated() {
            let label = UILabel()
            stackView.addArrangedAutolayoutSubview(label)
            apply(label) {
                $0.text = text
                if index == 0 {
                    $0.textColor = kDarkTextColor
                    $0.textAlignment = .left
                } else {
                    $0.textColor = kHeadingTitleTextColor
                    $0.textAlignment = .right
                }
                $0.font = UIFont(name: kMuliBold, size: 15)
            }
        }
        if let buttonTitle = self.headerButtonTitle {
            let contentView = UIView()
            
            let btn = UIButton()
            btn.setTitle(buttonTitle, for: .normal)
            btn.setTitleColor(UIColor(red: 255, green: 64, blue: 59), for: .normal)
            btn.titleLabel?.font = UIFont(name: kMuliBold, size: 14)
            btn.addTarget(self, action: #selector(self.headerButtonAction(sender:)), for: .touchUpInside)
            btn.contentHorizontalAlignment = .right
            
            let imageView = UIImageView(image: UIImage(named: "rightArrowRed"))
            contentView.addAutolayoutSubview(btn)
            contentView.addAutolayoutSubview(imageView)
            
            NSLayoutConstraint.activate([
                imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                imageView.widthAnchor.constraint(equalToConstant: 30),
                imageView.heightAnchor.constraint(equalToConstant: 30),
                
                btn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                btn.trailingAnchor.constraint(equalTo: imageView.leadingAnchor),
                btn.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ])
            stackView.addArrangedAutolayoutSubview(contentView)
        }
        
        return stackView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let localItem = item[indexPath.row]
        self.selectionDelegate?.cellSelected(index: indexPath.row, item: localItem)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let localItem = item[indexPath.row]
            self.swipeDelegate?.swipeToRemove(indexPath: indexPath, item: localItem)
        }
    }
    
    @objc func headerButtonAction(sender: UIButton){
        
    }
}

extension TopRecommendedTableView: RelatedItemCellDelegate {
    func selectquantity(sender: UIButton) {
        relatedItemsDelegate?.selectquantity(sender: sender)
    }
}

//
//  BannerCollectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 17/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

struct Image{
    var name: String?
    var url: String?
}

class BannerCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    weak var bannerDelegate: CellSelectionDelegate?
    var cellHeight: CGFloat? {
        didSet{
            self.translatesAutoresizingMaskIntoConstraints = false
            self.heightAnchor.constraint(equalToConstant: cellHeight!).isActive = true
        }
    }
    var items = [Image]() {
        didSet{
            self.reloadData()
        }
    }

    override init(frame: CGRect = .zero, collectionViewLayout layout: UICollectionViewLayout) {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        
        super.init(frame: .zero, collectionViewLayout: flow)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI(){
        self.register(UINib.init(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false
        self.backgroundColor = .clear
        self.isPagingEnabled = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        cell.bannerImageObj.layer.cornerRadius = 10
        let item = items[indexPath.row]
        if let imageName = item.name {
            cell.bannerImageObj.image = UIImage(named: imageName)
        }
        
        if let image = item.url {
            let url = URL(string: image)
            cell.bannerImageObj.kf.setImage(with: url)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FPThemeCollectionViewCell {
            cell.toggleSelected()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            bannerDelegate?.cellSelected(indexPath: indexPath,item: items[indexPath.row] as AnyObject)
        }
    }
    
}

extension BannerCollectionView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = self.cellHeight!

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

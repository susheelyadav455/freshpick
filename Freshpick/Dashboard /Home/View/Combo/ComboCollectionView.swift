//
//  ComboCollectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ComboCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    weak var comboSelectionDelegate: CellSelectionDelegate?
    var cellHeight: CGFloat? {
        didSet{
            self.translatesAutoresizingMaskIntoConstraints = false
            self.heightAnchor.constraint(equalToConstant: cellHeight!).isActive = true
        }
    }
    var items = [Image]() {
        didSet{
            self.reloadData()
        }
    }

    override init(frame: CGRect = .zero, collectionViewLayout layout: UICollectionViewLayout) {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        
        super.init(frame: .zero, collectionViewLayout: flow)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI(){
        self.register(UINib.init(nibName: "ComboCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ComboCollectionViewCell")
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false
        self.backgroundColor = .clear
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComboCollectionViewCell", for: indexPath) as! ComboCollectionViewCell
        let item = items[indexPath.row]
        if let imageName = item.name {
            cell.comboOfferImageObj.image = UIImage(named: imageName)
        }
        
        if let image = item.url {
            let url = URL(string: image)
            cell.comboOfferImageObj.kf.setImage(with: url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FPThemeCollectionViewCell {
            cell.toggleSelected()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            comboSelectionDelegate?.cellSelected(indexPath: indexPath,item: items[indexPath.row] as AnyObject)
        }
    }
    
}

extension ComboCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width*0.7
        let height = self.cellHeight!

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}

//
//  ComboCollectionViewCell.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ComboCollectionViewCell: UICollectionViewCell {

    @IBOutlet var comboOfferImageObj: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 12
    }

}

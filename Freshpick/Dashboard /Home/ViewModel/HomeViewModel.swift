//
//  HomeViewModel.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class HomeViewModel {
    private var deliveryResponse: DeliveryTimeModel!
    private var recommendedProducts: RecommendedProductsModel!
    private var orderHistoryModel: OrderHistoryModel!
    private var bannerModel: BannerModel!
    
    func getListDeliverySlots(controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        let url = API.BASEURL+API.DELIVERYTIME
        ServiceManager.shared.request(type: DeliveryTimeModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result {
                print("List Delivery Slots :\(result)")
                self.deliveryResponse = result
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func getListRecommendedProducts(controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        let url = API.BASEURL+API.RECOMMENDEDPRODUCT
        ServiceManager.shared.request(type: RecommendedProductsModel.self, url: url, method: .get, controller: controller) { (result) in
            if let result = result {
                print("List Recommended Products :\(result)")
                self.recommendedProducts = result
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func getOrderHistoryFor(userId: String, controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        let stringUrl = API.BASEURL+API.ORDERHISTORY
        var url = URL(string: stringUrl)!
        url.appendQueryItem(name: "userId", value: userId)
        ServiceManager.shared.request(type: OrderHistoryModel.self, url: url.absoluteString, method: .get, controller: controller) { (result) in
            if let result = result {
                print("List Order History :\(result)")
                self.orderHistoryModel = result
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func getBannersAndCombos(controller:UIViewController, completion:@escaping (_ error: String?)->Void){
        let stringUrl = API.BASEURL+API.BANNERANDCOMBOS
        ServiceManager.shared.request(type: BannerModel.self, url: stringUrl, method: .get, controller: controller) { (result) in
            if let result = result {
                print("List Banners And Combos :\(result)")
                self.bannerModel = result
                completion(nil)
            } else {
                completion("No Data Found")
            }
        }
    }
    
    func getListOfDeliveryOptions()->[DeliveryResponse]? {
        guard let group = self.deliveryResponse.response else { return nil}
        return group
    }
    
    func getListOfRecommendedProducts()->[InItem]? {
        guard let group = self.recommendedProducts.response else { return nil}
        switch group {
        case .string(_):
            return nil
        case .items(let item):
            return item
        }
//        return group
    }
    
    func getListOfPastProducts()->[InItem]? {
        guard let group = self.orderHistoryModel.response else { return nil}
        switch group {
        case .string(_):
            return nil
        case .items(let item):
            var historyItems = [InItem]()
            item.forEach {
                if let itemsPresent = $0.items {
                    historyItems.append(contentsOf: itemsPresent)
                }
            }
            return historyItems
        }        
    }
    
    func getBanners()->[BannerResponse]? {
        guard let response = self.bannerModel.response else { return nil}
        return response.filter { $0.responseFor == "discount" }
    }
    
    func getCombos()->[BannerResponse]? {
        guard let response = self.bannerModel.response else { return nil}
        return response.filter { $0.responseFor == "combos" }
    }
}
// discount combos

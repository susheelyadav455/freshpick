//
//  BannerOffersCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 12/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class BannerOffersCell: UICollectionViewCell {
    
    @IBOutlet weak var bannersOffersImageView: UIImageView!
    
}

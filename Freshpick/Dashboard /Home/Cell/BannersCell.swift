//
//  BannersCell.swift
//  Freshpick
//
//  Created by susheel chennaboina on 12/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class BannersCell: UICollectionViewCell {
    
    @IBOutlet weak var customViewObj: CustomView!
    @IBOutlet weak var bannerImageViewObj: UIImageView!
    
}

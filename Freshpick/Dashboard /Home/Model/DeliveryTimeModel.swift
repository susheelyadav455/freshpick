//
//  DeliveryTimeModel.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

// MARK: - DeliveryTimeModel
struct DeliveryTimeModel: Codable {
    let code: Int?
    let status: String?
    let response: [DeliveryResponse]?
}

// MARK: - Response
struct DeliveryResponse: Codable {
    let id: Int?
    let timeslot: String?
    let dateID: Int?
    let createdAt, updatedAt, date: String?

    enum CodingKeys: String, CodingKey {
        case id, timeslot
        case dateID = "dateId"
        case createdAt, updatedAt, date
    }
}

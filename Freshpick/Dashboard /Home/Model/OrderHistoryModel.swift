//
//  OrderHistoryModel.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

// MARK: - OrderHistoryModel
struct OrderHistoryModel: Codable {
    let code: Int?
    let status: String?
    let response: OrderHistoryResponse?
}

// MARK: - Response
struct OrderHistoryItems: Codable {
    let id, userID: Int?
    let orderID, status, paymentMode, addressID: String?
    let dateID, timeID, basketValue, deliveryCharge: String?
    let promoDiscount, total, createdAt, updatedAt: String?
    let items: [InItem]?
    let invoiceID: String?
    let allStatus: [String]?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "userId"
        case orderID = "orderId"
        case status, paymentMode
        case addressID = "addressId"
        case dateID = "dateId"
        case timeID = "timeId"
        case basketValue, deliveryCharge, promoDiscount, total, createdAt, updatedAt, items
        case invoiceID = "invoiceId"
        case allStatus
    }
}

enum OrderHistoryResponse: Codable {
    case string(String)
    case items([OrderHistoryItems])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([OrderHistoryItems].self) {
            self = .items(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(OrderHistoryModel.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .items(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

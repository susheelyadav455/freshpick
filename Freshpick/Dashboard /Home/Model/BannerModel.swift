//
//  BannerModel.swift
//  Freshpick
//
//  Created by Ajeet N on 19/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

// MARK: - BannerModel
struct BannerModel: Codable {
    let code: Int?
    let status: String?
    let response: [BannerResponse]?
}

// MARK: - Response
struct BannerResponse: Codable {
    let id: Int?
    let name: String?
    let bannerImage: String?
    let responseFor: String?
    let bannerType, typeID: String?
    let comboPrice, createdAt, updatedAt: String?
    let products: Products?

    enum CodingKeys: String, CodingKey {
        case id, name, bannerImage
        case responseFor = "for"
        case bannerType
        case typeID = "typeId"
        case comboPrice, createdAt, updatedAt, products
    }
}

//enum Products: Codable {
//    case productArray([Product])
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode([Product].self) {
//            self = .productArray(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        throw DecodingError.typeMismatch(Products.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Products"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .productArray(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}

struct Products: Codable {
    let productArray: [Product]?

    // Where we determine what type the value is
    init(from decoder: Decoder) throws {
        let container =  try decoder.singleValueContainer()

        // Check for a Double
        if let x = try? container.decode([Product].self) {
            productArray = x
            return
        }
        
        // Check for a Int
        if (try? container.decode(String.self)) != nil {
            productArray = nil
            return
        }
        throw DecodingError.typeMismatch(Products.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    // We need to go back to a dynamic type, so based on the data we have stored, encode to the proper type
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(productArray)
    }
}


// MARK: - Product
struct Product: Codable {
    let productID, productQuantityID: String?

    enum CodingKeys: String, CodingKey {
        case productID = "productId"
        case productQuantityID = "productQuantityId"
    }
}

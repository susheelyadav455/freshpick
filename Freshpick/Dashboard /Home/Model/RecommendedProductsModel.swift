//
//  RecommendedProductsModel.swift
//  Freshpick
//
//  Created by Ajeet N on 18/10/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

// MARK: - RecommendedProductsModel
struct RecommendedProductsModel: Codable {
    let code: Int?
    let status: String?
    let response: InItemResponse?
}

// MARK: - Response
//struct RecommendedProductResponse: Codable {
//    let productCode: Int?
//    let name: String?
//    let image: String?
//    let brandCode: String?
//    let quantity: ProductQuantity?
//    let isAssured: Bool?
//    let productQuantityCode: Int?
//    let weightOrQuantity: String?
//    let amount, actualAmount: Int?
//    let brand, subGroup, subGroupCode, groupCode: String?
//    let details, eanCode: String?
//    let disableReview, outOfStock, isApproved: Bool?
//    let donationType, donation: String?
//    let deleted: Bool?
//    let manufacturer: String?
//    let extraDiscount: Int?
//    let finalAmount: Double?
//
//    enum CodingKeys: String, CodingKey {
//        case productCode, name, image, brandCode, quantity, isAssured, productQuantityCode, weightOrQuantity, amount, actualAmount, brand, subGroup
//        case subGroupCode = "sub_groupCode"
//        case groupCode, details
//        case eanCode = "EANCode"
//        case disableReview, outOfStock, isApproved, donationType, donation, deleted, manufacturer, extraDiscount, finalAmount
//    }
//}



//
//  HomeVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import UIKit


class HomeVC: ViewController {
    @IBOutlet var scrollContentView: UIView!
    
    private var pastOrderHeight: NSLayoutConstraint!
    private var recommendedOrderHeight: NSLayoutConstraint!
    @IBOutlet var scrollViewBottomAnchor: NSLayoutConstraint!
    
    //MARK: - View Models
    private let viewModel = HomeViewModel()
    private let groupViewModel = GroupViewModel()
    private let filterViewModel = FilterViewModel()
    
    //MARK: - Subviews
    private let homeStackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 16)
    private var shopByCategory: CategoryView!
    private var shopByBrands: CategoryView!
    private let topRecomendation = TopRecommendedTableView()
    private var pastOrder = PastOrderItemsView()
    private let searchBar:FPSearchBar = .fromNib()
    private let bannerImageCrausal = BannerCollectionView()
    private let deliveryCollectionView = StandardDeliveryView()
    private let comboCollectionView = ComboCollectionView()
    private let deliveryAddressView: DeliveryView = .fromNib()
    
    private var trackView: TrackOrderNavigationView = {
        let view = TrackOrderNavigationView()
        return view
    }()

    //MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        shopByCategory = CategoryView(delegate: self, productCategory: CategoryType.category)
        shopByBrands = CategoryView(delegate: self, productCategory: CategoryType.brands)
        trackView.delegate = self

        ConstantValues.shared.userId = "2"
        view.backgroundColor = UIColor(red: 248, green: 248, blue: 248)

        addSubViews()
        applyModel()
        addAutoLayoutToSubviews()
        callGetApis()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func callGetApis(){
        viewModel.getListDeliverySlots(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if let group = self.viewModel.getListOfDeliveryOptions(), error == nil {
                self.deliveryCollectionView.items = group
                self.deliveryCollectionView.isHidden = false
            }
        }
        viewModel.getListRecommendedProducts(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if let group = self.viewModel.getListOfRecommendedProducts(), error == nil {
                self.topRecomendation.item = group
                self.topRecomendation.isHidden = false
                DispatchQueue.main.async {
                    group.forEach { _ in
                        self.view.layoutIfNeeded()
                        self.recommendedOrderHeight.constant = self.topRecomendation.contentSize.height
                    }
                }
            }
        }
        viewModel.getOrderHistoryFor(userId: ConstantValues.shared.userId, controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if let group = self.viewModel.getListOfPastProducts(), error == nil {
                self.pastOrder.item = group
                self.pastOrder.isHidden = false
                DispatchQueue.main.async {
                    group.forEach { _ in
                        self.view.layoutIfNeeded()
                        self.pastOrderHeight.constant = self.pastOrder.contentSize.height
                    }
                }
            }
        }
        groupViewModel.getGroups(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if error == nil {
                let items = self.groupViewModel.getGroups()
                self.shopByCategory.items = items.map({
                    CategoryItems(name: $0.name, details: nil, image: nil, urlImage: $0.featureImage)
                })
                self.shopByCategory.isHidden = false
            }
        }
        filterViewModel.getBrands(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if error == nil {
                let brands = self.filterViewModel.getBrands()
                self.shopByBrands.items = brands.map({
                    CategoryItems(name: $0.name, details: nil, image: nil, urlImage: $0.image)
                })
                self.shopByBrands.isHidden = false
            }
        }
        viewModel.getBannersAndCombos(controller: self) {[weak self] (error) in
            guard let self = self else { return }
            if error == nil {
                let banners = self.viewModel.getBanners()
                let combos = self.viewModel.getCombos()

                let bannerImages = banners?.map({ Image(name: nil, url: $0.bannerImage) }).compactMap({ $0 })
                let combosImages = combos?.map({ Image(name: nil, url: $0.bannerImage) }).compactMap({ $0 })

                if let banerImages = bannerImages, !banerImages.isEmpty {
                    self.bannerImageCrausal.items = banerImages
                    self.bannerImageCrausal.isHidden = false
                }
                if let comboImages = combosImages, !comboImages.isEmpty {
                    self.comboCollectionView.items = comboImages
                    self.comboCollectionView.isHidden = false
                }
            }
        }
    }
    
    private func addSubViews() {
        self.scrollContentView.addAutolayoutSubview(homeStackView)
        self.view.addAutolayoutSubview(trackView)
        
        [deliveryAddressView, searchBar, deliveryCollectionView, bannerImageCrausal, comboCollectionView, shopByCategory, topRecomendation, shopByBrands, pastOrder].compactMap({ $0 }).forEach {
            homeStackView.addArrangedAutolayoutSubview($0)
        }
        
        [deliveryCollectionView, bannerImageCrausal, comboCollectionView, shopByCategory, topRecomendation, shopByBrands, pastOrder].compactMap({ $0 }).forEach {
            $0.isHidden = true
        }
    }
    
    private func applyModel() {
        apply(searchBar) {
            $0.fpSearchBar = self
            $0.setShadow(shadowRadius: 5)
            $0.navigateOnBecomeFirstResponder = true
        }
        apply(deliveryCollectionView) {
            $0.cellHeight = 90
        }
        apply(bannerImageCrausal) {
            $0.cellHeight = 114
        }
        apply(comboCollectionView) {
            $0.cellHeight = 160
        }
        apply(shopByCategory) {
            $0.numberOfColumns = 3
            $0.cellHeight = 173
            $0.visibleCells = 6
            $0.delegate = self
            $0.isHeaderVisible = true
            $0.isOfferViewHidden = true
            $0.headerTitle = "Shop by Categories"
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        }
        apply(topRecomendation) {
            $0.separatorStyle = .none
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
            $0.relatedItemsDelegate = self
            $0.headerTitles = ["Top Recommendations"]
            $0.selectionDelegate = self
            $0.swipeDelegate = self
        }
        apply(shopByBrands) {
            $0.numberOfColumns = 3
            $0.cellHeight = 173
            $0.visibleCells = 9
            $0.delegate = self
            $0.isHeaderVisible = true
            $0.isOfferViewHidden = true
            $0.headerTitle = "Shop by Brands"
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
        }        
        apply(pastOrder) {
            $0.separatorStyle = .none
            $0.backgroundColor = UIColor(red: 248, green: 248, blue: 248)
            $0.headerTitles = ["Your Past Orders"]
            $0.headerButtonTitle = "See All"
            $0.selectionDelegate = self
        }
    }
    
    private func addAutoLayoutToSubviews() {
        recommendedOrderHeight = topRecomendation.heightAnchor.constraint(equalToConstant: topRecomendation.contentSize.height)
        pastOrderHeight = pastOrder.heightAnchor.constraint(equalToConstant: pastOrder.contentSize.height)
        
        scrollViewBottomAnchor.constant = 70
        NSLayoutConstraint.activate([
            searchBar.heightAnchor.constraint(equalToConstant: 40),
            deliveryAddressView.heightAnchor.constraint(equalToConstant: 90),
            pastOrderHeight,
            recommendedOrderHeight,
            homeStackView.topAnchor.constraint(equalTo: scrollContentView.topAnchor, constant: 20),
            homeStackView.leadingAnchor.constraint(equalTo: scrollContentView.leadingAnchor, constant: 16),
            homeStackView.trailingAnchor.constraint(equalTo: scrollContentView.trailingAnchor, constant: -16),
            homeStackView.bottomAnchor.constraint(equalTo: scrollContentView.bottomAnchor, constant: -20),
        ])
        
        NSLayoutConstraint.activate([
            trackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
//            trackView.bottomAnchor.constraint(equalTo: scrollContentView.bottomAnchor, constant: -20),
            trackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            trackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            trackView.heightAnchor.constraint(equalToConstant: 70)
        ])
    }
}

extension HomeVC: RelatedItemCellDelegate {
    func selectquantity(sender: UIButton) {
        
    }
}

extension HomeVC: FPSearchBarDelegate{
    func searchBarShouldBeginEditing(_ textField: UITextField, _ shouldEdit: Bool) {
        if !shouldEdit {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func searchButtonClicked(_ text: String) {
        print("HomeVC Search Button clicked :\(text)")
    }
    
    func searchBarText(_ text: String) {
        print("HomeVC Search text :\(text)")
    }
}

extension HomeVC: PastOrderCellSelected {
    func cellSelected(index: Int, item: InItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        vc.item = item
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeVC: CategoryCollectionViewSelection {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType) {
        print("HomeVC category selection index :\(index) items :\(item) productCategory :\(productCategory)")
        if productCategory == .brands {
            // Call Brands API call
            let filterDic = ["brand":["\(item.name!)"], "discount": [], "price": []] as [String : Any]
            print("Filter Dictionary is :\(filterDic)")
            self.filterViewModel.postFilter(param: filterDic, view: view) {[weak self] (result) in
                if let items = result {
                    //  Navigate to Product List
                    let productListVC = self?.storyboard?.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
                    productListVC.itemList = items
                    productListVC.callAPI = false
                    productListVC.navigationBarTitle = item.name
                    self?.navigationController?.pushViewController(productListVC, animated: true)
                } else {
                    self?.toast(msgString: "\(item.name!) has no products", view: self?.view ?? UIView())
                }
            }
        } else if productCategory == .category {
            let productListVC = self.storyboard?.instantiateViewController(identifier: "SubCategoryListViewController") as! SubCategoryListViewController
            productListVC.groupViewModel = self.groupViewModel
            productListVC.subcategoryIndex = index
            productListVC.navigationBarTitle = self.groupViewModel.getGroups()[index].name
            self.navigationController?.pushViewController(productListVC, animated: true)
        }
    }
}

extension HomeVC: SwipeToRemoveDelegate {
    func swipeToRemove(indexPath: IndexPath, item: InItem) {
        debugPrint("Swipe :\(indexPath.row)")
        debugPrint("Swipe :\(String(describing: item.name))")        
    }
}

extension HomeVC: CategoryViewDelegate {
    func selectAll(sender: UIButton, categoryType: CategoryType) {
        switch categoryType {
        case .brands:
            
            let productListVC = UIStoryboard.init(name: "Others", bundle: .main).instantiateViewController(withIdentifier: "AllCategoriesViewController") as! AllCategoriesViewController //AllCategoriesViewController()
            productListVC.itemList = self.shopByBrands.items
            productListVC.navigationBarTitle = "All Brands"
//            productListVC.groupViewModel = self.filterViewModel
            productListVC.filterViewModel = self.filterViewModel
            productListVC.viewType = categoryType
            self.navigationController?.pushViewController(productListVC, animated: true)
            
            break
        case .category:
            let productListVC = UIStoryboard.init(name: "Others", bundle: .main).instantiateViewController(withIdentifier: "AllCategoriesViewController") as! AllCategoriesViewController //AllCategoriesViewController()
            productListVC.itemList = self.shopByCategory.items
            productListVC.navigationBarTitle = "All Category"
            productListVC.groupViewModel = self.groupViewModel
            productListVC.viewType = categoryType
            self.navigationController?.pushViewController(productListVC, animated: true)
            break
        case .search:
            break
        case .offers:
            break
        }
    }
}

extension HomeVC: TrackOrderDelegate {
    func didTapTrackOrder() {
        print("Track Order clicked")
        let vc = TrackOrderViewController()
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
}

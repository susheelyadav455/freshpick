//
//  FPSearchBar.swift
//  Freshpick
//
//  Created by Ajeet N on 20/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol FPSearchBarDelegate: AnyObject {
    func searchBarText(_ text: String)
    func searchButtonClicked(_ text: String)
    func searchBarShouldBeginEditing(_ textField: UITextField,_ shouldEdit: Bool)
}

class FPSearchBar: UIView {
    weak var fpSearchBar: FPSearchBarDelegate?
    var navigateOnBecomeFirstResponder = false
    @IBOutlet var searchIcon: UIImageView!
    @IBOutlet var searchTextField: UITextField!{
        didSet{
            searchTextField.delegate = self
            searchTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        }
    }
    @IBOutlet var clearButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        clearButton.addTarget(self, action: #selector(self.clearButtonAction(sender:)), for: .touchUpInside)
        clearButton.isHidden = true
    }
    
    @objc private func clearButtonAction(sender: UIButton){
        self.searchTextField.text = nil
        self.clearButton.isHidden = true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, text.count>0 {
            self.fpSearchBar?.searchBarText(text)
            self.clearButton.isHidden = false
        } else {
            self.clearButton.isHidden = true
        }
    }
}

extension FPSearchBar: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = textField.resignFirstResponder()
        self.fpSearchBar?.searchButtonClicked(textField.text ?? "")
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.fpSearchBar?.searchBarShouldBeginEditing(textField, !navigateOnBecomeFirstResponder)
        return !navigateOnBecomeFirstResponder
    }
}

//
//  CustomNavigationView.swift
//  Freshpick
//
//  Created by Ajeet N on 20/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CustomNavigationView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    var leadingButtonTitle: String?{
        didSet{
            leadingButton.setTitle("Cancel", for: .normal)
            leadingButton.isHidden = false
        }
    }
    var trailingButtonTitle: String?{
        didSet{
            trailingButton.setTitle("Cancel", for: .normal)
            trailingButton.isHidden = false
        }
    }
    var navigationBarTitle: String?{
        didSet{
            titleLabel.text = navigationBarTitle
            titleLabel.isHidden = false
        }
    }
    
    private let leadingButton = UIButton()
    private let trailingButton = UIButton()
    private let titleLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    var navigationBarHeight = 60 {
        didSet{
            heightConstrain.constant = CGFloat(navigationBarHeight)
        }
    }
    private var heightConstrain = NSLayoutConstraint()
    
    private func setupUI(){
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .horizontal, spacing: 0)
        self.addAutolayoutSubview(stackView)
        [leadingButton,titleLabel,trailingButton].forEach {
            stackView.addArrangedAutolayoutSubview($0)
        }
        
        titleLabel.setContentHuggingPriority(.init(rawValue: 249), for: .horizontal)
        titleLabel.font = UIFont(name: kMuliBold, size: 17)
        titleLabel.textAlignment = .center
        
        leadingButton.titleLabel?.font = UIFont(name: kMuliMedium, size: 17)
        leadingButton.setTitleColor(UIColor(red: 255, green: 64, blue: 59), for: .normal)
        
        trailingButton.titleLabel?.font = UIFont(name: kMuliMedium, size: 17)
        trailingButton.setTitleColor(UIColor(red: 255, green: 64, blue: 59), for: .normal)
        
        heightConstrain = self.heightAnchor.constraint(equalToConstant: CGFloat(navigationBarHeight))

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            heightConstrain,
            leadingButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.22, constant: 0),
            trailingButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.22, constant: 0),
        ])
    }
}

//
//  CategoryView.swift
//  Freshpick
//
//  Created by Ajeet N on 19/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol CategoryViewDelegate: AnyObject {
    func selectAll(sender: UIButton, categoryType: CategoryType)
}

class CategoryView: UIView {
    private var collectionView: CategoryCollectionView!
    weak var delegate: CategoryViewDelegate?
    var items = [CategoryItems](){
        didSet{
            updateConstrain()
            collectionView.items = items
            self.collectionView.reloadData()
        }
    }
    
    var numberOfColumns: CGFloat = 3{
        didSet{
            updateConstrain()
            collectionView.numberOfColumns = numberOfColumns
            self.collectionView.reloadData()
        }
    }
    
    var cellHeight: CGFloat = 173{
        didSet{
            updateConstrain()
            collectionView.cellHeight = cellHeight
            self.collectionView.reloadData()
        }
    }
    
    var isHeaderVisible = false {
        didSet{
            headerView.isHidden = !isHeaderVisible
        }
    }
    
    var headerTitle: String?{
        didSet{
            headerView.headerViewTitle.text = headerTitle
        }
    }
    
    var headerButtonTitle: String?{
        didSet{
            headerView.headerViewButtonObj.setTitle(headerButtonTitle, for: .normal)
            headerView.buttonImageObj.isHidden = headerButtonTitle == nil ? true : false
        }
    }
    
    var isOfferViewHidden = true{
        didSet{
            collectionView.isOfferViewHidden = isOfferViewHidden
            self.collectionView.reloadData()
        }
    }
    
    var visibleCells: Int? {
        didSet{
            updateConstrain()
            collectionView.displayCells = visibleCells ?? 0
        }
    }
    
    var heightConstrain = NSLayoutConstraint()
    private var categoryType: CategoryType?
    private let headerView: CategoryHeaderView = .fromNib()
    
    init(delegate: CategoryCollectionViewSelection, productCategory: CategoryType) {
        self.categoryType = productCategory
        self.collectionView = CategoryCollectionView(delegate: delegate, productCategory: productCategory)
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI(){
        self.backgroundColor = .clear
        addStackView()
        addCollectionView()
    }
    
    private func addStackView(){
        let stackView = UIStackView(alignment: .fill, distribution: .fill, axis: .vertical, spacing: 0)
        self.addAutolayoutSubview(stackView)
        
        [headerView,collectionView].forEach {
            stackView.addArrangedAutolayoutSubview($0)
        }
        
        headerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        headerView.isHidden = !isHeaderVisible
        headerView.headerViewButtonObj.addTarget(self, action: #selector(self.seeAllAction(sender:)), for: .touchUpInside)
    }
    
    @objc private func seeAllAction(sender: UIButton) {
        guard let category = self.categoryType else { return }
        delegate?.selectAll(sender: sender, categoryType: category)
    }
    
    private func addCollectionView(){
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.clipsToBounds = true
        collectionView.layer.cornerRadius = 10
        collectionView.numberOfColumns = self.numberOfColumns
        collectionView.cellHeight = self.cellHeight
        collectionView.items = items
        
        var x = Double(items.count)/Double(numberOfColumns)
        x.round(.awayFromZero)
        heightConstrain = collectionView.heightAnchor.constraint(equalToConstant: CGFloat(x)*cellHeight)
        NSLayoutConstraint.activate([
            heightConstrain,
        ])
    }
    
    private func updateConstrain(){
        if let initialHeight = visibleCells {
            var x = Double(initialHeight)/Double(numberOfColumns)
            x.round(.awayFromZero)
            heightConstrain.constant = CGFloat(x)*cellHeight
        } else {
            var x = Double(items.count)/Double(numberOfColumns)
            x.round(.awayFromZero)
            heightConstrain.constant = CGFloat(x)*cellHeight
        }
    }
}

//
//  CategoryCollectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 17/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
struct CategoryItems {
    var name: String?
    var details: String?
    var image: String?
    var urlImage: String?
}

enum CategoryType {
    case brands
    case category
    case search
    case offers
}

protocol CategoryCollectionViewSelection: AnyObject {
    func cellSelected(index: Int, item: CategoryItems, productCategory: CategoryType)
}

class CategoryCollectionView: UICollectionView {
    var items = [CategoryItems](){
        didSet{
            self.reloadData()
        }
    }
    
    var displayCells: Int = 0 {
        didSet{
            self.reloadData()
        }
    }
    
    var numberOfColumns: CGFloat = 3
    var cellHeight: CGFloat = 173
    var isOfferViewHidden = true
    private var selectionDelegate: CategoryCollectionViewSelection?
    private var productCategory: CategoryType!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init(delegate: CategoryCollectionViewSelection, productCategory: CategoryType) {
        self.selectionDelegate = delegate
        self.productCategory = productCategory
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .vertical
        flow.minimumInteritemSpacing = 0;
        flow.minimumLineSpacing = 0;
        
        super.init(frame: .zero, collectionViewLayout: flow)
        isScrollEnabled = false
        backgroundColor = .clear
        setupUI()
    }
    
    private func setupUI() {
        self.register(UINib.init(nibName: "SearchSuggestionCell", bundle: nil), forCellWithReuseIdentifier: "SearchSuggestionCell")
        self.delegate = self
        self.dataSource = self
    }
}

extension CategoryCollectionView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var numberOfCellToDisplay = 0
        if displayCells > 0 {
            if displayCells < items.count {
                numberOfCellToDisplay = displayCells
            } else {
                numberOfCellToDisplay = items.count
            }
        } else {
            numberOfCellToDisplay = items.count
        }
        return numberOfCellToDisplay
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchSuggestionCell", for: indexPath) as! SearchSuggestionCell
        let item = items[indexPath.row]
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        cell.offerObj.isHidden = self.isOfferViewHidden
        cell.titleObj.text = item.name
        //
        if !self.isOfferViewHidden {
            cell.offerObj.text = item.details
        }
        if let imageName = item.image {
            cell.imageObj.image = UIImage(named: imageName)
        }
        if let imageUrl = item.urlImage {
            let url = URL(string: imageUrl)
            cell.imageObj.kf.setImage(with: url)
        }
        return cell
    }
}

extension CategoryCollectionView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.frame.width/CGFloat(numberOfColumns)
        let height = cellHeight
        
        return CGSize(width: width, height: height)
    }
}

extension CategoryCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        selectionDelegate?.cellSelected(index: indexPath.row, item: item, productCategory: self.productCategory)
    }
}

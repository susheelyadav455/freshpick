//
//  CategoryHeaderView.swift
//  Freshpick
//
//  Created by Ajeet N on 19/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class CategoryHeaderView: UIView {
    @IBOutlet var headerViewTitle: UILabel!
    @IBOutlet var headerViewButtonObj: UIButton!
    @IBOutlet var buttonImageObj: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setupUI(){
        backgroundColor = UIColor(red: 248, green: 248, blue: 248)
    }
    
}

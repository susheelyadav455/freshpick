//
//  PresentationTableViewCell.swift
//  Freshpick
//
//  Created by Ajeet N on 14/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class PresentationTableViewCell: UITableViewCell {

    @IBOutlet var radioButtonObj: UIButton!
    @IBOutlet var quantityTextObj: UILabel!
    @IBOutlet var savedTextObj: UILabel!
    @IBOutlet var discountedPrice: UILabel!
    @IBOutlet var originalPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        discountedPrice.textColor = UIColor(red: 101, green: 101, blue: 101)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
//            radioButtonObj.setTitleColor(UIColor(red: 39, green: 45, blue: 47), for: .normal)
//            radioButtonObj.backgroundColor = UIColor(red: 39, green: 45, blue: 47)
            radioButtonObj.isSelected = true
            quantityTextObj.textColor = UIColor(red: 39, green: 45, blue: 47)
            savedTextObj.textColor = UIColor(red: 39, green: 45, blue: 47)
            originalPrice.textColor = UIColor(red: 39, green: 45, blue: 47)


        } else {
//            radioButtonObj.setTitleColor(UIColor(red: 181, green: 181, blue: 181), for: .normal)
//            radioButtonObj.backgroundColor = UIColor(red: 181, green: 181, blue: 181)

            radioButtonObj.isSelected = false
            quantityTextObj.textColor = UIColor(red: 181, green: 181, blue: 181)
            savedTextObj.textColor = UIColor(red: 181, green: 181, blue: 181)
            originalPrice.textColor = UIColor(red: 181, green: 181, blue: 181)
        }
    }

}

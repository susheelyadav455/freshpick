//
//  PresentViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 13/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol PresentationDelegate:class {
    func selectedItem(item: Quantity)
    func proceedAction(sender: UIButton)
}

class PresentViewController: UIViewController {
    @IBOutlet var whiteViewObj: UIView!
//    @IBOutlet var availableQuantityObj: UILabel!
    @IBOutlet var nameObj: UILabel!
    @IBOutlet var proceedButtonObj: UIButton!
    var items = [Quantity]()
    var selectedItem: Int?
    var name: String?
    
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    weak var presentDelegate: PresentationDelegate?

    
    
    @IBOutlet var tableViewObj: UITableView!{
        didSet{
            self.tableViewObj.delegate = self
            self.tableViewObj.dataSource = self
            self.tableViewObj.isScrollEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameObj.text = self.name
        print("Name is : \(String(describing: self.name))")
        print("Items count is : \(self.items.count)")
        print("selectedItem is : \(String(describing: self.selectedItem))")
        print("presentDelegate is : \(String(describing: self.presentDelegate))")

        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        whiteViewObj.layoutIfNeeded()

        whiteViewObj.roundCorner(corners: [.topRight, .topLeft], radius: 30)
        proceedButtonObj.layer.cornerRadius = 8
//        tableViewHeight.constant = tableViewObj.contentSize.height
        print("table view content size before:\(tableViewObj.contentSize.height)")
        
        print("table view content size after:\(tableViewObj.contentSize.height)")
        tableViewHeight.constant = tableViewObj.contentSize.height
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.gestureRecognizer(sender:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
        
    }

    @IBAction func proceedButtonAction(_ sender: UIButton) {
        self.presentDelegate?.proceedAction(sender: sender)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func gestureRecognizer(sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
}

extension PresentViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        self.presentDelegate?.selectedItem(item: item)
    }
}

extension PresentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresentationTableViewCell", for: indexPath) as! PresentationTableViewCell
        let item = items[indexPath.row]
        cell.discountedPrice.text = "Rs.\(item.finalAmount?.price ?? 0.0)"
        cell.originalPrice.attributedText = "Rs.\(item.actualAmount ?? 0)".strikeThrough()
        cell.savedTextObj.text = "Save: Rs. \(item.savedAmount ?? "")"
        cell.quantityTextObj.text = "\(item.quantity ?? "")"
        
        if item.id == selectedItem {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
        
//        {
//            "id": 5,
//            "productCode": "5",
//            "quantity": "500g",
//            "savedAmount": "5",
//            "deleted": false,
//            "actualAmount": 30,
//            "finalAmount": 25,
//            "createdAt": "2020-09-03T17:00:12.000Z",
//            "updatedAt": "2020-09-03T17:00:12.000Z"
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
}

extension PresentViewController:UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: whiteViewObj) ?? false {
            return false
        }
        return true
    }
}

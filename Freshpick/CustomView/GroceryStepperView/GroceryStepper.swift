//
//  GroceryStepper.swift
//  CustomStepper
//
//  Created by Ajeet N on 10/08/20.
//  Copyright © 2020 Ajeet N. All rights reserved.
//

import UIKit
protocol StepperDelegate:AnyObject {
    func numberOfItems(count: Int)
}

class GroceryStepper: UIView {
    weak var stepperDelegate: StepperDelegate?
    private var defValue = 0 {
        didSet{
            changeUIForNewValue()
        }
    }
    
    private let decrementButton:UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(red: 249/255, green: 199/255, blue: 71/255, alpha: 1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 8
        btn.setImage(UIImage.init(named: "down"), for: .normal)
        btn.adjustsImageWhenHighlighted = false
        btn.tag = 1
        
        return btn
    }()
    
    private let incrementButton:UIButton = {
        let btn2 = UIButton(type: .custom)
        btn2.backgroundColor = UIColor(red: 249/255, green: 199/255, blue: 71/255, alpha: 1)
        btn2.translatesAutoresizingMaskIntoConstraints = false
        btn2.layer.cornerRadius = 8
        btn2.setImage(UIImage.init(named: "up"), for: .normal)
        btn2.adjustsImageWhenHighlighted = false
        btn2.setContentHuggingPriority(.init(rawValue: 270), for: .horizontal)
        btn2.tag = 2

        return btn2
    }()
    
    private let label: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.setContentHuggingPriority(.init(rawValue: 249), for: .horizontal)
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func changeUIForNewValue() {
        if defValue > 0 {
            decrementButton.isHidden = false
            label.isHidden = false
        } else {
            decrementButton.isHidden = true
            label.isHidden = true
        }
        label.text = "\(defValue)"
    }
    
    private func setupUI() {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 254/255, green: 241/255, blue: 191/255, alpha: 1)
        layer.cornerRadius = 12
        addAutolayoutSubview(backgroundView)
        
        let stack = UIStackView()
        backgroundView.addAutolayoutSubview(stack)
        apply(stack) {
            $0.axis = .horizontal
            $0.distribution = .fillEqually
            $0.alignment = .fill
        }
        
        changeUIForNewValue()
        decrementButton.addTarget(self, action: #selector(self.incrementDecrementAction(sender:)), for: .touchUpInside)
        incrementButton.addTarget(self, action: #selector(self.incrementDecrementAction(sender:)), for: .touchUpInside)
        
        stack.addArrangedSubview(decrementButton)
        stack.addArrangedSubview(label)
        stack.addArrangedSubview(incrementButton)
        
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            backgroundView.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor),
//            backgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: backgroundView.topAnchor),
            stack.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor),
            stack.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor)
        ])
    }
    
    @objc func incrementDecrementAction(sender: UIButton) {
        if sender.tag == 1 {
            // dec
            defValue -= 1
        } else if sender.tag == 2 {
            // inc
            defValue += 1
        }
        stepperDelegate?.numberOfItems(count: defValue)
    }
}

//
//  ImageCrausalCollectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 09/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol CrausalScrollingDelegate: class {
    func movedTo(pageNo: Int)
}
class ImageCrausalCollectionView: UICollectionView {
    var images = [String?](){
        didSet{
            self.reloadData()
        }
    }
    weak var crDelegate: CrausalScrollingDelegate?
    
    init(frame: CGRect) {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        flow.minimumInteritemSpacing = 0;
        flow.minimumLineSpacing = 0;
        
        super.init(frame: frame, collectionViewLayout: flow)
        //self.images = crausalImages
        setupUI()
    }
    
    init() {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        flow.minimumInteritemSpacing = 0;
        flow.minimumLineSpacing = 0;
        
        super.init(frame: .zero, collectionViewLayout: flow)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI() {
        self.isPagingEnabled = true
        self.showsHorizontalScrollIndicator = false

        self.register(UINib.init(nibName: "ImageCrausalCell", bundle: nil), forCellWithReuseIdentifier: "ImageCrausalCell")
        self.delegate = self
        self.dataSource = self
    }

}

extension ImageCrausalCollectionView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCrausalCell", for: indexPath) as! ImageCrausalCell
        if let image = images[indexPath.row]{
            let url = URL(string: image)
            cell.crausalImage.kf.setImage(with: url)
        } else {
            cell.crausalImage.image = UIImage(named: images[indexPath.row] ?? "")
        }
        return cell
    }
}

extension ImageCrausalCollectionView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.layoutIfNeeded()
        self.backgroundColor = .white
        
        let width = self.frame.width
        let height = self.frame.height
        
        return CGSize(width: width, height: height)
        
    }
}

extension ImageCrausalCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\(indexPath.row) item is selected")
    }
}

extension ImageCrausalCollectionView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentIndex = self.contentOffset.x / self.frame.width
        crDelegate?.movedTo(pageNo: Int(currentIndex))
    }
}

//
//  ImageCrausalView.swift
//  Freshpick
//
//  Created by Ajeet N on 09/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class ImageCrausalView: UIView {
    private var imageCrausalCollectionView = ImageCrausalCollectionView()
    private var pageController = UIPageControl()
    var images: [String?]? {
        didSet{
            imageCrausalCollectionView.images = images!
            updateModel()
//            imageCrausalCollectionView.reloadData()
        }
    }

        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        imageCrausalCollectionView = ImageCrausalCollectionView(frame: .zero)
        imageCrausalCollectionView.crDelegate = self
        imageCrausalCollectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageCrausalCollectionView)
        
        
        addSubview(pageController)
        
        NSLayoutConstraint.activate([
            pageController.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            pageController.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            pageController.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        
        NSLayoutConstraint.activate([
            imageCrausalCollectionView.topAnchor.constraint(equalTo: self.topAnchor),
            imageCrausalCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageCrausalCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageCrausalCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
            imageCrausalCollectionView.heightAnchor.constraint(equalToConstant: 300)
        ])
    }
}

extension ImageCrausalView{
    func updateModel(){
        pageController.translatesAutoresizingMaskIntoConstraints = false
        pageController.numberOfPages = images!.count
        pageController.currentPage = 0
        pageController.pageIndicatorTintColor = kPageControllerColor
        pageController.currentPageIndicatorTintColor = kRedColor
    }
}

extension ImageCrausalView: CrausalScrollingDelegate {
    func movedTo(pageNo: Int) {
        pageController.currentPage = pageNo
    }
}

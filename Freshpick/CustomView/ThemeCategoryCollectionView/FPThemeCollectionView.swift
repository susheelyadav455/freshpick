//
//  FPThemeCollectionView.swift
//  Freshpick
//
//  Created by Ajeet N on 14/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol CellSelectionDelegate: class {
    func cellSelected(indexPath: IndexPath, item: AnyObject)
}

class FPThemeCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    weak var fpThemeDelegate: CellSelectionDelegate?
    
    var items = [String]() {
        didSet{
            self.reloadData()
        }
    }

    override init(frame: CGRect = .zero, collectionViewLayout layout: UICollectionViewLayout) {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        flow.minimumInteritemSpacing = 0;
        flow.minimumLineSpacing = 0;
        
        super.init(frame: .zero, collectionViewLayout: flow)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupUI(){
        self.register(UINib.init(nibName: "FPThemeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FPThemeCollectionViewCell")
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false
        self.backgroundColor = .clear
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FPThemeCollectionViewCell", for: indexPath) as! FPThemeCollectionViewCell
        cell.titleObj.text = items[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FPThemeCollectionViewCell {
            cell.toggleSelected()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            fpThemeDelegate?.cellSelected(indexPath: indexPath,item: items[indexPath.row] as AnyObject)
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FPThemeCollectionViewCell {
            cell.toggleSelected()
        }
    }
}

extension FPThemeCollectionView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //self.layoutIfNeeded()
        
        let label = UILabel(frame: CGRect.zero)
        label.text = items[indexPath.item]
        label.sizeToFit()

        //self.backgroundColor = .white
        let width = label.frame.width+20
        let height = self.frame.height

        return CGSize(width: width, height: height)
    }
}

//
//  FPThemeCollectionViewCell.swift
//  Freshpick
//
//  Created by Ajeet N on 14/08/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class FPThemeCollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleObj: UILabel!
    @IBOutlet var indicatorView: UIView!
    
    override var isSelected: Bool{
        didSet{
            toggleSelected()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell(){
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        titleObj.backgroundColor = .clear
        indicatorView.isHidden = true
        indicatorView.backgroundColor = UIColor(red: 255, green: 64, blue: 59)
    }
    
    func toggleSelected ()
    {
        if (self.isSelected){
            indicatorView.isHidden = false
            titleObj.textColor = UIColor(red: 255, green: 64, blue: 59)
        }else {
            titleObj.textColor = UIColor(red: 181, green: 181, blue: 181)
            indicatorView.isHidden = true
        }
    }

}

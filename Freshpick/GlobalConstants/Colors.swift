//
//  Colors.swift
//  Freshpick
//
//  Created by Ajeet N on 03/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

protocol Colors {
    
}

extension Colors {
    var themeRedTextColor: UIColor {
        return UIColor(red: 255/255, green: 64/255, blue: 59/255, alpha: 1)
    }
//    rgb 39 45 47
    var themeBlackTextColor: UIColor {
        return UIColor(red: 39/255, green: 45/255, blue: 47/255, alpha: 1)
    }
}

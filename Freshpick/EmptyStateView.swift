//
//  EmptyStateView.swift
//  Freshpick
//
//  Created by Ajeet N on 14/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit
protocol EmptyStateProtocol: AnyObject {
    func buttonAction(sender: UIButton)
}

class EmptyStateView: UIView {
    weak var delegate: EmptyStateProtocol?
    
    @IBOutlet var placeholderViewObj: UIView!
    @IBOutlet var imageViewObj: UIImageView!
    @IBOutlet var titleLabelObj: UILabel!
    @IBOutlet var buttonObj: UIButton!
    @IBOutlet var buttonPlaceholderObj: UIView!
    
    enum ViewType {
        case myOrders(String,UIImage,String?)
        case offers(String,UIImage,String?)
        case promoCode(String,UIImage,String?)
        case address(String,UIImage,String?)
        case notification(String,UIImage,String?)
        case noProductsFound(String,UIImage,String?)
    }
    
    var viewType: ViewType?{
        didSet{
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buttonPlaceholderObj.roundCorner(corners: [.topLeft,.topRight], radius: 14)
        buttonObj.addTarget(self, action: #selector(self.buttonAction(sender:)), for: .touchUpInside)
    }
    
    private func setupUI() {
        placeholderViewObj.setShadow()
        buttonObj.layer.cornerRadius = 10
    }
    
    private func updateUI() {
        switch viewType! {
        
        case .myOrders(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        case .offers(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        case .promoCode(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        case .address(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        case .notification(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        case .noProductsFound(let titleText, let image, let buttonTitle):
            titleLabelObj.text = titleText
            imageViewObj.image = image
            if let buttonTitle = buttonTitle {
                buttonObj.setTitle(buttonTitle, for: .normal)
            } else {
                buttonPlaceholderObj.isHidden = true
            }
            break
        }
    }
    
    @objc private func buttonAction(sender: UIButton) {
        self.delegate?.buttonAction(sender: sender)
    }
}


//
//  TestViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 14/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    override func loadView() {
        super.loadView()
        let view = UIView()
        view.backgroundColor = .yellow
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let empView = view.addEmptyState(type: .address("You don’t have any saved address, add new address below.", UIImage(named: "addressEmptyState")!, "ADD NEW ADDRESS")) as! EmptyStateView
        empView.delegate = self
    }
}

extension TestViewController: EmptyStateProtocol {
    func buttonAction(sender: UIButton) {
        print("Empty state button tapped")
    }
}


//
//  TrackOrderModel.swift
//  Freshpick
//
//  Created by Ajeet N on 24/12/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let trackOrderModel = try? newJSONDecoder().decode(TrackOrderModel.self, from: jsonData)

import Foundation

// MARK: - TrackOrderModel
struct TrackOrderModel: Codable {
    let response: TrackOrderResponse?
}

// MARK: - Response
struct TrackOrderResponse: Codable {
    let orderID, dateAndTime, deliveryAddress, totalItems: String?
    let totalSaving, totalPaid: String?
    let progressStatus: Int?
}

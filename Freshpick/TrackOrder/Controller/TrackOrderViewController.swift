//
//  TrackOrderViewController.swift
//  Freshpick
//
//  Created by Ajeet N on 25/12/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class TrackOrderViewController: UIViewController, Colors {
    override func loadView() {
        super.loadView()
        let view = UIView()
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let btn1 = UIButton(type: .custom)
        btn1.setTitle("Cancel", for: .normal)
        print("Color : \(themeRedTextColor)")
        btn1.setTitleColor(themeRedTextColor, for: .normal)
        btn1.addTarget(self, action: #selector(self.trackOrderCancelTapped(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setLeftBarButton(item1, animated: true)
        self.navigationItem.title = "Recent Order"
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:themeBlackTextColor]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        let trackView: TrackOrderView = UIView.fromNib()
        view.addAutolayoutSubview(trackView)
        
        NSLayoutConstraint.activate([
            trackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            trackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            trackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            trackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    @objc func trackOrderCancelTapped(_ sender: UIButton) {
        print("Cancel clicked")
    }
}

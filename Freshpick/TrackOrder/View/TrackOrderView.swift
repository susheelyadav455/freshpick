//
//  TrackOrderView.swift
//  Freshpick
//
//  Created by Ajeet N on 09/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

class TrackOrderView: UIView {
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var dateAndTimeLabel: UILabel!
    @IBOutlet var helpStackView: UIStackView!
    @IBOutlet var deliveryAddressObj: UILabel!
    @IBOutlet var totalItemsObj: UILabel!
    @IBOutlet var totalSavedObj: UILabel!
    @IBOutlet var totalPaidObj: UILabel!
    @IBOutlet var inProcessImage: UIImageView!
    @IBOutlet var inProcessText: UILabel!
    @IBOutlet var packedImage: UIImageView!
    @IBOutlet var packedText: UILabel!
    @IBOutlet var outOfDeliveryImage: UIImageView!
    @IBOutlet var outForDeliveryText: UILabel!
    @IBOutlet var deliveredImage: UIImageView!
    @IBOutlet var deliveredText: UILabel!
    @IBOutlet var viewOrderDetailsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        viewOrderDetailsButton.layer.cornerRadius = 10
    }
}

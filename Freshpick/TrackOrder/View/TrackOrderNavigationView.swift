//
//  TrackOrderView.swift
//  Freshpick
//
//  Created by Ajeet N on 03/01/21.
//  Copyright © 2021 Susheel Chennaboina. All rights reserved.
//

import UIKit

protocol TrackOrderDelegate: AnyObject {
    func didTapTrackOrder()
}

class TrackOrderNavigationView: UIView {
    init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    @available(*,unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    weak var delegate: TrackOrderDelegate?
    private var backGroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "trackViewBackground")
        imageView.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        imageView.contentMode = .scaleAspectFit
        //        imageView.widthAnchor.constraint(equalToConstant: 22).isActive = true
        return imageView
    }()
    
    private func setupUI() {
        addAutolayoutSubview(backGroundImage)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.addGestureRecognizer(tapGesture)
        
        NSLayoutConstraint.activate([
            backGroundImage.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            backGroundImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            backGroundImage.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            backGroundImage.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor)        ])
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer) {
        print("tap")
        self.delegate?.didTapTrackOrder()
    }
    
}

//
//  NetworkCall.swift
//  Freshpick
//
//  Created by susheel chennaboina on 05/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import Alamofire


class ServiceManager: NSObject {
    static let shared = ServiceManager()
    
    
    //MARK:- Network Call without Parameters
    func request<T:Decodable>(type:T.Type, url: String, method: HTTPMethod, controller:UIViewController, completion completionHandler:@escaping(T?) -> Void) {
        
        if Connectivity.isConnectedToInternet {
            var activityIndicator = UIActivityIndicatorView()
            print("The url in service manager is :\(String(describing: url))")
            DispatchQueue.main.async {
                controller.view.isUserInteractionEnabled = false
                activityIndicator = self.showActivityIndicator(view: controller.view)
            }
            guard let url = URL(string: url) else { return }
            let headers = ["Content-Type": "application/json","Accept":"application/json"]
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = TimeInterval(30)
            configuration.timeoutIntervalForRequest = TimeInterval(30)
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            sessionManager.request(url, method: method, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch (response.result) {
                case .success:
                    guard let json = response.data else { return }
                    do{
                        let result = try JSONDecoder().decode(T.self, from: json)
                        DispatchQueue.main.async {
                            controller.view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                        }
                        completionHandler(result)
                    }catch{
                        print(error)
                        DispatchQueue.main.async {
                            controller.view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                            controller.popupAlert(title: "Error", message: Errors.decode.rawValue, actionTitles: ["OK"], actions: [
                                nil
                            ])
                        }
                    }
                    break
                case .failure( _):
                    DispatchQueue.main.async {
                        controller.popupAlert(title: "Error", message: Errors.serverError.rawValue, actionTitles: ["OK"], actions: [
                            nil
                        ])
                        controller.view.isUserInteractionEnabled = true
                        self.removeActivityindicator(indicator: activityIndicator)
                    }
                    break
                }
            }.session.finishTasksAndInvalidate()
            
        } else {
            DispatchQueue.main.async {
                controller.popupAlert(title: "Error", message: Errors.noInternet.rawValue, actionTitles: ["OK"], actions: [
                    nil
                ])
            }
        }
    }
    
    
    
    //MARK:- Network Call with Request Parameters
    func request<T:Decodable>(type:T.Type, url:String, method: HTTPMethod, view:UIView, parameters: [String:Any]?, completion completionHandler:@escaping(T?) -> Void) {
        if Connectivity.isConnectedToInternet {
            view.isUserInteractionEnabled = false
            var activityIndicator = UIActivityIndicatorView()
            DispatchQueue.main.async {
                activityIndicator = self.showActivityIndicator(view: view)
            }
            guard let url = URL(string: url) else { return }
            let headers = ["Content-Type": "application/json","Accept":"application/json"]
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = TimeInterval(30)
            configuration.timeoutIntervalForRequest = TimeInterval(30)
            let sessionManager = Alamofire.SessionManager(configuration: configuration)
            print("ServiceManager url is:\(url)")
            sessionManager.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
                switch (response.result) {
                case .success:
                    guard let json = response.data else { return }
                    do{
                        let result = try JSONDecoder().decode(T.self, from: json)
                        DispatchQueue.main.async {
                            view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                        }
                        completionHandler(result)
                    }catch{
                        print(error)
                        
                        DispatchQueue.main.async {
                            view.isUserInteractionEnabled = true
                            self.removeActivityindicator(indicator: activityIndicator)
                        }
                        self.toast(msgString: Errors.decode.rawValue, view: view)
                    }
                    break
                case .failure( _):
                    if response.response?.statusCode == nil{
                        self.toast(msgString: "Unable to connect server, Please try again..!", view: view)
                    }else{
                        if response.response?.statusCode == 404{
                            self.toast(msgString: "Something went wrong..", view: view)
                        }else if response.response?.statusCode == 500{
                            self.toast(msgString: "Session Expired, Please login again..!", view: view)
                            NotificationCenter.default.post(name: Notification.Name("SessionExpired"), object: nil)
                        }
                    }
                    DispatchQueue.main.async {
                        view.isUserInteractionEnabled = true
                        self.removeActivityindicator(indicator: activityIndicator)
                    }
                    break
                }
            }.session.finishTasksAndInvalidate()
            
        } else {
            self.toast(msgString: Errors.noInternet.rawValue, view: view)
            
        }
    }
    
}

//
//  APIs.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation

struct API {
    static let BASEURL = "https://basketpe.com/api/v1/"
    
    
    static let LOGIN = "otp/generate"
    static let OTPVERIFY = "otp/verify"
    static let SEARCH = "g/search"
    static let PRODUCT = "product"
    static let ADDRESS = "user/address"
    static let GETGROUPS = "group"
    static let GETBRANDS = "brand"
    static let GETPRODUCTQUANTITY = "product/quantity"
    static let GETCART = "cart"
    static let ADDTOCART = "cart/add"
    static let POSTFILTER = "g/filter"
    static let OFFERS = "offer" //https://basketpe.com/api/v1/offer/
    
    //  Home Controller APIs
    static let DELIVERYTIME = "delivery/time"
    static let RECOMMENDEDPRODUCT = "recommendation"
    static let ORDERHISTORY = "orders"
    static let BANNERANDCOMBOS = "banner"
}

extension URL {
    mutating func appendQueryItem(name: String, value: String?) {
        guard var urlComponents = URLComponents(string: absoluteString) else { return }
        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        // Create query item
        let queryItem = URLQueryItem(name: name, value: value)
        // Append the new query item in the existing query items array
        queryItems.append(queryItem)
        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems
        // Returns the url from new url components
        self = urlComponents.url!
    }
}

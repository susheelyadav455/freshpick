//
//  Errors.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation

enum Errors: String {
    case noInternet  = "No Internet access"
    case decode = "Unable to Decode"
    case serverError = "Server busy, Please Try again later . ! "
    
}

//
//  OTPViewController.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var signInOutlet: CustomButton!
    
    var mobileNumber: String?
    let otpViewModelObj = OTPViewModel()
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstTextField.layer.borderColor = UIColor.white.cgColor
        self.secondTextField.layer.borderColor = UIColor.white.cgColor
        self.thirdTextField.layer.borderColor = UIColor.white.cgColor
        self.fourthTextField.layer.borderColor = UIColor.white.cgColor

        self.firstTextField.layer.borderWidth = 1.0
        self.secondTextField.layer.borderWidth = 1.0
        self.thirdTextField.layer.borderWidth = 1.0
        self.fourthTextField.layer.borderWidth = 1.0

        
        self.firstTextField.layer.cornerRadius = 5.0
        self.secondTextField.layer.cornerRadius = 5.0
        self.thirdTextField.layer.cornerRadius = 5.0
        self.fourthTextField.layer.cornerRadius = 5.0

        debugPrint("the received mobile number in OTP VC is :\(String(describing: mobileNumber))")

    }
    
    
    //MARK: SignIn Button Action
    @IBAction func signInButtonAction(_ sender: Any) {
        
        if firstTextField.text == "" || secondTextField.text == "" || thirdTextField.text == "" || fourthTextField.text == "" {
            self.toast(msgString: "Please enter valid OTP", view: self.view)
        }
        else {
            
            let firstData  = firstTextField.text!
            let secondData = secondTextField.text!
            let thirdData  = thirdTextField.text!
            let fourthData = fourthTextField.text!
            
            let otpData = firstData + secondData + thirdData + fourthData
            let enteredPinValue = otpData
            
            let receivedMobileNumber = mobileNumber ?? ""
        
            let optParameters = ["mob": receivedMobileNumber, "otp": enteredPinValue ]
            debugPrint("The otp parameters are :\(optParameters)")
            otpViewModelObj.getOTPResponse(controller: self, parameters: optParameters) {(response) in
                debugPrint("The response in OTP VC is :\(response)")
                if response.code == 200 {
                    debugPrint("Navigate to dashboard")
                    self.toast(msgString: "Logged in Succesfully", view: self.view)
                }
                else if response.code == 500 {
                    self.toast(msgString: "Please enter Correct OTP. OTP doesn't match", view: self.view)

                }
                
            }
        }
        
    }
    
    
    //MARK: Resend Button Action
    @IBAction func resendOTPButtonAction(_ sender: Any) {
        
        
    }
    
    //MARK: Wrong Mobile Number
    @IBAction func wrongMobileNumber(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

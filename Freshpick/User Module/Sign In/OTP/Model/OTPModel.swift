//
//  OTPModel.swift
//  Freshpick
//
//  Created by susheel chennaboina on 08/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import Foundation


struct OTPModel: Codable {
    let code: Int?
    let response: UserData?
    let error: String?
}

// MARK: - Response
struct UserData: Codable {
    let id: Int?
    let mob_no: String?
}


// Sucess
//{
//    "code": 200,
//    "response": {
//        "id": 5,
//        "name": null,
//        "email": null,
//        "password": null,
//        "deleted": false,
//        "mob_no": "9014888455",
//        "createdAt": "2020-09-05T06:14:47.000Z",
//        "updatedAt": "2020-09-05T06:14:47.000Z"
//    }
//}


// Failure

//{
//    "code": 500,
//    "error": "OTP not match"
//}

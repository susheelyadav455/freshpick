//
//  OTP View Model.swift
//  Freshpick
//
//  Created by susheel chennaboina on 08/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import UIKit


class OTPViewModel {
    
    func getOTPResponse(controller:UIViewController, parameters: [String: Any], completion:@escaping (_ response: OTPModel)-> Void) {
        
        let otpUrl = API.BASEURL + API.OTPVERIFY
        ServiceManager.shared.request(type: OTPModel.self, url: otpUrl, method: .post, view: controller.view, parameters: parameters) { (result) in
            debugPrint("The result in otp view model is :\(String(describing: result))")
            if let resultObj = result {
                completion(resultObj)
            }
        }
   }
}

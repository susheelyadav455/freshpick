//
//  ViewController.swift
//  Freshpick
//
//  Created by susheel chennaboina on 02/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//


import UIKit

class ViewController: UIViewController {

   @IBOutlet weak var numberTextField: UITextField!
   @IBOutlet weak var sendOTPOutlet: CustomButton!
    
    let loginViewModelObj = LoginViewModel()
    

    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
       // UISetUp()
}
    
   //MARK: UITextField Setup
    func UISetUp() {
            numberTextField.layer.borderColor = UIColor.white.cgColor
            numberTextField.attributedPlaceholder = NSAttributedString(string: "Enter Your Mobile Number",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            numberTextField.layer.borderWidth = 1.0
            numberTextField.layer.cornerRadius = 5.0
    }
    
    //MARK: Send OTP Button Action
    @IBAction func sendOTPButtonAction(_ sender: Any) {
        
        if self.numberTextField.text == "" {
            self.toast(msgString: "Please enter your mobile number", view: self.view)
        }
        else {

            let number = self.numberTextField.text ?? ""
            let  loginParams = ["mob": number]
            print("The login params is :\(loginParams)")
            
        loginViewModelObj.getLoginResponse(controller: self, parameters: loginParams) { (result) in
        print("the login response in vc is :\(String(describing: result.response))")
                
                if result.code == 200 {
                    DispatchQueue.main.async {
                        let otpVc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        otpVc.mobileNumber = String(describing: number)
                        self.navigationController?.pushViewController(otpVc, animated: true)
                    }
                }
            }
        }
    }
    
}


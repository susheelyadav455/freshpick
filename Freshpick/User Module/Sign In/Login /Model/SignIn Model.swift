//
//  SignIn Model.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation


struct SignInModel : Codable {
     let code: Int?
    let response: String?
    
}

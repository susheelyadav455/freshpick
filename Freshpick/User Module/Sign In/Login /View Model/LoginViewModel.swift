//
//  LoginViewModel.swift
//  Freshpick
//
//  Created by susheel chennaboina on 07/09/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import Foundation
import UIKit


class LoginViewModel {
    
    func getLoginResponse(controller:UIViewController, parameters: [String: Any], completion:@escaping (_ response: SignInModel)-> Void) {
         let loginUrl = API.BASEURL + API.LOGIN
        ServiceManager.shared.request(type: SignInModel.self, url: loginUrl, method: .post, view: controller.view, parameters: parameters) { (result) in
            debugPrint("The parameters received in login model is :\(String(describing: parameters))")
            print("The result of login api in view model is :\(String(describing: result))")
            if let resultObj = result {
                completion(resultObj)
            }
            
        }

    }
    
}

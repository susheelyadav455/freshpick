//
//  SignupVC.swift
//  Freshpick
//
//  Created by susheel chennaboina on 06/07/20.
//  Copyright © 2020 Susheel Chennaboina. All rights reserved.
//

import UIKit


class SignupVC: UIViewController {

    
//MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        let newBackButton = UIBarButtonItem(title: "back", style: .plain, target: self, action: #selector(backButtonAction))
        newBackButton.image = UIImage(named: "back-2")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    
//MARK: Back Button Action
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "Muli-ExtraBold", size: 32) ??
                                         UIFont.systemFont(ofSize: 30)]
        } else {
            // Fallback on earlier versions
        }
    }
    
}
